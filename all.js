
/**
 * @classdesc Objet angle
 * @author Vincent Audergon
 * @version 1.0
 */
class Angle {

    /**
     * Constructeur de l'objet Angle
     * @param {double} alpha L'angle alpha (intérieur au polygone)
     * @param {double} beta L'angle beta (exterieur au polygone)
     * @param {string[]} points Les points concercées par l'angle (ABC par ex.)
     * @example
     *
     *      new Angle(90,270,['A','B','C'])
     */
    constructor(alpha, beta, points){
        this.alpha = alpha;
        this.beta = beta;
        this.points = points;
    }

}
/**
 * @classdesc Objet Line, représente une droite en mathématiques
 * @author Vincent Audergon
 * @version 1.0
 */
class Line {

    /**
     * Constructeur de l'objet Line. Il est possible de créer la droite à partir
     * d'un angle et d'un point ou des attributs a et b d'une fonction affine.
     * @param {double|double} angle L'angle de la fonction en **radians** | a si fromAngle est faux
     * @param {Point|double} p1 Un {@link Point} de la fonction | b si fromAngle est faux
     * @param {boolean} [fromAngle=true] Argument facultatif, si la droite est calculée
     *                            à partir d'un angle ou pas, vrai par défaut
     *
     * @example
     *
     *      let l1 = new Line(1,1, false); //A partir de a et b
     *      let l2 = new Line(45,new Point(1,1)) //A partir d'un angle
     */
    constructor(angle, p1, fromAngle = true) {
        if (fromAngle) {
            if (angle % Math.PI !== Math.PI / 2) {
                this.a = Math.round(Math.tan(angle) * 100) / 100;
                this.b = Math.round((p1.y - this.a * p1.x) * 100) / 100;
            } else {
                this.a = Infinity;
                this.b = p1.x;
            }
        } else {
            this.a = angle;
            this.b = p1;
        }
    }

    /**
     * Calcule la coordonnée y en fonction de la coordonnée x
     * Selon l'équation *y = ax+b*
     * @param {double} x La coordonnée x
     * @return {double} La coordonnée y
     */
    calcY(x) {
        if (this.a != Infinity) {
            return this.a * x + this.b;
        } else {
            return x === this.b ? Infinity : NaN;
        }
    }

    /**
     * Vérifie si un point est sur la droite ou non
     * @param {Point} point Le {@link Point} à vérifier
     * @return {boolean} Si le point est sur la droite
     */
    containsPoint(point) {
        return this.calcY(point.x) === point.y || this.calcY(point.x) === Infinity;
    }
}
/**
 * @classdesc Bean node. Noeud qui compose une grille de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Node {

    /**
     * Constructeur de Node
     * @param {double} x La position x du noeud
     * @param {double} y La position y du noeud
     * @param {double} width La largeur et hauteur du noeud
     * @param {DrawingGrid} refGrid La référence vers la {@link DrawingGrid} (grille de dessin)
     */
    constructor(x, y, width, refGrid) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.refGrid = refGrid;
        this.init();
    }

    /**
     * Initialise le noeud
     */
    init() {

        this.graphics = new PIXI.Graphics();
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.beginFill(0xFFFFFF);
        this.graphics.drawRect(this.x-10,this.y-10, this.width+20,this.width+20);
        this.graphics.endFill(0xFFFFFF);
        this.graphics.on('pointerdown', this.onNodeClicked.bind(this));

        this.point = new PIXI.Graphics();
        this.point.beginFill(0x000000);
        this.point.drawRect(this.x, this.y, this.width, this.width);
        this.point.endFill(0x000000);
        this.point.interactive = true;
        this.point.buttonMode = true;
        this.point.on('pointerdown', this.onNodeClicked.bind(this));
    }

    /**
     * Retourne les coordonées du centre du noeud
     * @return {Point} le centre du noeud
     */
    center() {
        return new Point(this.x + this.width / 2, this.y + this.width / 2);
    }

    /**
     * Méthode de callback appelée lorsqu'un click est détecté sur le noeud
     * @param {Event} e L'événement JavaScript
     */
    onNodeClicked(e) {
        this.refGrid.onNodeClicked(e, this);
    }

    /**
     * Méthode de callback appelée lorsque le click est enfoncé
     * et que le curseur passe au dessus du noeud
     * @deprecated
     * @param {Event} e L'événement JavaScript
     */
    onNodeEncountered(e) {
        this.refGrid.onNodeEncountered(e, this)
    }


}
/**
 * @classdesc Bean point
 * @author Vincent Audergon
 * @version 1.0
 */
class Point {

    /**
     * Constructeur de l'objet point
     * @param {double} x la composante x du point
     * @param {double} y la composante y du point
     * @param {boolean} [nonconvex=false] indique si l'angle lié au point est convexe ou non (facultatif, faux par défaut)
     */
    constructor(x, y, nonconvex = false) {
        this.x = x;
        this.y = y;
        this.nonconvex = nonconvex;
    }

    /**
     * Additionne les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    add(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }

    /**
     * Soustrait les composantes x et y d'un autre point au point pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    sub(point) {
        return new Point(this.x - point.x, this.y - point.y);
    }

    /**
     * Multiplie les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    mult(point) {
        return new Point(this.x * point.x, this.y * point.y);
    }

    /**
     * Divise les composantes x et y du point par celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    div(point) {
        return new Point(this.x / point.x, this.y / point.y);
    }

    /**
     * Rend un nouveau point avec les valeurs absolues du point (par ex. 1;-1 => 1;1)
     * @return {Point} le nouveau {@link Point}
     */
    abs(){
        return new Point(Math.abs(this.x), Math.abs(this.y));
    }

    /**
     * Indique si deux points sont égaux ou non (composantes égales entre elles)
     * @param {Point} point le deuxième {@link Point}
     * @return {boolean} si les points sont égaux
     */
    equals(point) {
        return this.x === point.x && this.y === point.y;
    }
}
/**
 * @classdesc Bean Question
 * @author Vincent Audergon
 * @version 1.0
 */
class Question {

    /**
     * Constructeur de Question
     * @param {string[]} label La consigne de la question dans les différentes langues
     * @param {JSON} responses Les réponses possibles
     * @param {JSON} traps Les réponses piège
     * @param {int} type Le type de la question (0 => QCM, 1 => dessin)
     * @param {string} qcmtype Le type de QCM (radio | checkbox)
     * @example
     *
     *      let q1 = new Question({fr: "En français", de:"En allemand"},
     *                            {value:"rect", response: true},
     *                            {value:"square", response: false},
     *                            0, "radio")
     */
    constructor(label, responses, traps, type, qcmtype) {
        this.label = label;
        this.responses = responses;
        this.traps = traps;
        this.type = type;
        this.qcmtype = qcmtype;
    }

}
/**
 * @classdesc Bean shape, représente un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class Shape {

    /**
     * Constructeur de l'objet Shape
     * @param {double} x La coordonnée x du polygone sur le canvas
     * @param {double} y La coordonnée y du polygone sur le canvas
     * @param {Point[]} pts La liste des {@link Point} qui composents le polygone
     * @param {double} scale La taille de la forme sur le canvas
     * @param {string} id L'id du polygone
     * @param {string[]} names Le nom de la formes dans toutes les langues
     * @param {Point} [origin={x:0,y:0}] Le {@link Point} d'origine du polygone (facultatif, 0;0 par défaut)
     * @param {number} harmos Degré harmos
     */
    constructor(x, y, pts, scale, id, names, origin, harmos) {
        /** @type {PIXI.Graphics} les graphismes du polygone */
        this.graphics = new PIXI.Graphics();
        this.graphics.x = x;
        this.graphics.y = y;
        /** @type {Point[]} les points qui servent à calculer les propriétés du polygone */
        this.pts = pts;
        /** @type {double} l'ordre de grandeur du polygone (au niveau graphique) */
        this.scale = scale;
        /** @type {string[]} l'id de la forme */
        this.id = id;
        /** @type {string[]} les noms de la forme dans les différentes langues */
        this.names = names;
        /** @type {Point} l'origine du polygone */
        this.origin = origin || new Point(0, 0);
        /** @type {boolean} si les propriétés du polygone ont été calculées */
        this.initialized = false;

        //Propriétés du polygone
        /** @type {Point[]} les points du polygone */
        this.points = [];
        /** @type {Vector[]} les vecteurs qui composent le polygone */
        this.vectors = [];
        /** @type {Angle[]} les angles du polygone */
        this.angles = [];
        /** @type {double[]} les côtés du polygone */
        this.sides = { count: 0 };
        /** @type {int} le nombre de paires de côtés parallèles */
        this.parallels = 0;
        /** @type {Point} le centre de gravité du polygone */
        this.gravityPoint = new Point(0,0);
        /** @type {int} le nombre d'axes de symétrie */
        this.symAxis = 0;
        /** @type {boolean} si le polygone est convexe */
        this.convex = true;
        /** @type {int} le niveau harmos */
        this.harmos = harmos;
        /** @type {Line[]} les médiatrices du polygone */
        this.sideBisections = [];
        /** @type {Line[]} le(s) axe de symétrie du polygone */
        this.axeSym = [];
        /** @type {Line[]} le(s) médianes du polygone */
        this.mediane = [];
        /** @type {Line[]} les bisectrices du polygone */
        this.bisections = [];

        this.nAxeSym = 0;
        this.nPaireCotePara = 0;

        this.gravity = [];
    }

    /**
     * Calcules les différentes propriétés du polygone
     */
    init() {
        //console.log("Start1");

        let lastVect = new Vector;
        let pointSupp = false;
        this.gravityPoint = VectorUtils.calcGravityPoint(this.pts);

        //Calcul du vecteur, de la médiatrice et des points

        //console.log("Pts avant modif:");


        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            let thirdPoint = null;
            if (i + 2 > this.pts.length){
                thirdPoint = this.pts[1];
            } else if (i + 2 > this.pts.length - 1){
                thirdPoint = this.pts[0];
            } else {
                thirdPoint = this.pts[i + 2];
            }

            let firsVect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);
            let secondVect = new Vector(thirdPoint.x - secondPoint.x, thirdPoint.y - secondPoint.y,
                new Point(secondPoint.x, secondPoint.y), secondPoint.name, thirdPoint.name);

            if (firsVect.fullDirection === secondVect.fullDirection) {
                delete this.pts[i + 1];

                for (let j = i + 1; j < this.pts.length - 1; j++) {
                    this.pts[j] = this.pts[j + 1]
                }
                this.pts.pop();
                i--;
            }
        }

        let alphabet = {0:"A",1:"B",2:"C",3:"D",4:"E",5:"F",6:"G",7:"H",8:"I",9:"J",10:"K"};
        for (let i = 0; i < this.pts.length; i++) {
            this.pts[i].name = alphabet[i];
        }

        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];

            let vect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);

            this.sides.count++;
            this.sides[currentPoint.name + secondPoint.name] = vect.norme;
            let sideBisection = VectorUtils.calcSideBisection(vect);
            if (sideBisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.sideBisections[`m${currentPoint.name.toLowerCase()}`] = sideBisection;
            this.vectors[currentPoint.name.toLowerCase()] = vect;
            this.points[currentPoint.name] = new Point(currentPoint.x, currentPoint.y, currentPoint.nonconvex);
            if (currentPoint.nonconvex) this.convex = false;
        }


        for (let i = 0; i < this.pts.length; i++) {
            let lastPoint = new Point;
            lastPoint = this.pts[(i - 1 >= 0) ? i - 1 : this.pts.length - 1];

            let lastVector = this.vectors[lastPoint.name.toLowerCase()];
            let currentPoint = this.pts[i];
            let currentVector = this.vectors[currentPoint.name.toLowerCase()];

            let secondPoint = new Point;
            secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            //Calcul de l'angle;
            let alpha = VectorUtils.calcAngle(this.vectors[currentPoint.name.toLowerCase()], this.vectors[lastPoint.name.toLowerCase()],false);
            if (currentPoint.nonconvex) alpha = 360 - alpha;
            let angle = new Angle(alpha, 360 - alpha, [lastPoint, currentPoint, secondPoint]);
            this.angles[lastPoint.name + currentPoint.name + secondPoint.name] = angle;
            //Calcul des bissectrices
            let bisection = VectorUtils.calcBisection(angle, lastVector, currentVector);
            if (bisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.bisections[`b${currentPoint.name.toLowerCase()}`] = bisection;
            //Calcul des parallèles
            if (this.vectors[lastPoint.name.toLowerCase()].direction === this.vectors[secondPoint.name.toLowerCase()].direction) {
                this.parallels++;
            }
        }
        this.parallels /= 2;
        this.initialized = true;
        this.graphics = this.initGraphics();
    }

    /**
     * Retourne un Sprite du polygone. Si une largeur et une hauteur maximale sont données,
     * le sprite sera dimensionné pour rentrer dans ces limites (le plus proche possible)
     * @param {double} maxwidth La largeur maximale du sprite généré (facultatif)
     * @param {double} maxheight La hauteur maximale du sprite généré (facultatif)
     * @param {boolean} lines Défini si les différents axes du polygones doivent apparaître (facultatif)
     * @return {PIXI.Sprite} le sprite du polygone
     */
    initGraphics(maxwidth, maxheight, lines, nAxeSym = 0, nPaireCotePara = 0, axeSym = [], bissec = [], mediatrice = [], mediane = [], gravity = [], affAxeSym = 0, affBissec = 0, affMediatrice = 0, affMediane = 0, affPointNom = false, affgravity = false) {
        let graphics = this.generateGraphics();
        this.axeSym = axeSym;
        this.nAxeSym = nAxeSym;
        this.nPaireCotePara = nPaireCotePara;
        this.bisections = bissec;
        this.sideBisections = mediatrice;
        this.mediane = mediane;
        this.gravity = gravity;

        if (maxwidth && maxheight) {

            let oldscale = this.scale;
            let wscale = this.scale * (maxwidth / graphics.width);
            let hscale = this.scale * (maxheight / graphics.height);
            this.scale = wscale > hscale ? hscale : wscale;
            graphics = this.generateGraphics(lines, affAxeSym, affBissec, affMediatrice, affMediane, affPointNom, affgravity);
            this.scale = oldscale;
        }
        graphics.anchor.set(0.5);
        graphics.interactive = true;
        graphics.buttonMode = true;
        return graphics;
    }

    /**
     * Génère un sprite à partir des points du polygone.
     * Il est possible de donner une liste de droites à afficher sur le polygone.
     * @param {Line[]} lines La liste des {@link Line} à afficher (facultatif)
     * @return {PIXI.Sprite} le sprite généré
     */
    generateGraphics(lines = false, affAxeSym = 0, affBissec = 0, affMediatrice = 0, affMediane = 0, affPointNom = false, affgravity = false) {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(this.getRandomColor());
        graphics.lineStyle(1, 0x00, 1);
        graphics.moveTo(this.origin.x, this.origin.y);
        let nomPoint = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        let index = 0;

        for (let point in this.points) {
            graphics.lineTo(this.points[point].x * this.scale, this.points[point].y * this.scale);

            if (affPointNom){
                let pointNom = new PIXI.Text(nomPoint[index], {fontFamily: 'Arial', fontSize: 15, fill: 0x00, align: 'left'});
                index++;
                pointNom.position.x = this.points[point].x * this.scale;
                pointNom.position.y = this.points[point].y * this.scale - 10;
                graphics.addChild(pointNom);
            }
        }
        graphics.lineTo(this.origin.x, this.origin.y);
        graphics.endFill();
        let polygon = new PIXI.Sprite(graphics.generateCanvasTexture());
        if (lines) {

            if (affMediatrice !== 0){
                // Affiche la ou les médiatrices
                if (affMediatrice === -1){
                    // Affiche toutes les médiatrices
                    for (let i = 0; i < this.sideBisections.length; i++) {
                        let sBis = this.sideBisections[i];
                        let x1 = sBis["x1"];
                        let y1 = sBis["y1"];
                        let x2 = sBis["x2"];
                        let y2 = sBis["y2"];

                        graphics.lineStyle(3,0x0099ff,1);
                        graphics.beginFill(0x0099ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiatrice
                    let sBis = this.sideBisections[affMediatrice - 1];
                    let sbisNom = new PIXI.Text(sBis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sBis["x1"];
                    let y1 = sBis["y1"];
                    let x2 = sBis["x2"];
                    let y2 = sBis["y2"];

                    graphics.lineStyle(3,0x0099ff,1);
                    graphics.beginFill(0x0099ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    sbisNom.x = (x1 + 0.02) * this.scale;
                    sbisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(sbisNom);

                    graphics.endFill();
                }
            }

            if (affBissec !== 0){
                // Affiche toutes les bissectrices
                if (affBissec === -1){
                    for (let i = 0; i < this.bisections.length; i++) {
                        let bis = this.bisections[i];

                        let x1 = bis["x1"];
                        let y1 = bis["y1"];
                        let x2 = bis["x2"];
                        let y2 = bis["y2"];

                        graphics.lineStyle(3,0x33cc33,1);
                        graphics.beginFill(0x33cc33);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une bissectrice
                    let bis = this.bisections[affBissec - 1];
                    let bisNom = new PIXI.Text(bis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = bis["x1"];
                    let y1 = bis["y1"];
                    let x2 = bis["x2"];
                    let y2 = bis["y2"];

                    graphics.lineStyle(3,0x33cc33,1);
                    graphics.beginFill(0x33cc33);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    bisNom.x = (x1 + 0.02) * this.scale;
                    bisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(bisNom);

                    graphics.endFill();
                }
            }

            if (affMediane !== 0){
                if (affMediane === -1){
                    // Affiche toutes les médianes
                    for (let i = 0; i < this.mediane.length; i++) {
                        let med = this.mediane[i];

                        let x1 = med["x1"];
                        let y1 = med["y1"];
                        let x2 = med["x2"];
                        let y2 = med["y2"];

                        graphics.lineStyle(3,0xcc00ff,1);
                        graphics.beginFill(0xcc00ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiane
                    let med = this.mediane[affMediane - 1];
                    let medNom = new PIXI.Text(med["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = med["x1"];
                    let y1 = med["y1"];
                    let x2 = med["x2"];
                    let y2 = med["y2"];

                    graphics.lineStyle(3,0xcc00ff,1);
                    graphics.beginFill(0xcc00ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    medNom.x = (x1 + 0.02) * this.scale;
                    medNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(medNom);

                    graphics.endFill();
                }
            }

            if (affAxeSym !== 0){
                if (affAxeSym === -1){
                    // Affiche les axes de symétrie
                    for (let i = 0; i < this.axeSym.length; i++) {
                        let sym = this.axeSym[i];

                        let x1 = sym["x1"];
                        let y1 = sym["y1"];
                        let x2 = sym["x2"];
                        let y2 = sym["y2"];

                        graphics.lineStyle(2,0xbf4080,1);
                        graphics.beginFill(0xbf4080);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    }
                } else {
                    // Affiche un axe de symétrie
                    let sym = this.axeSym[affAxeSym - 1];
                    let symNom = new PIXI.Text(sym["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sym["x1"];
                    let y1 = sym["y1"];
                    let x2 = sym["x2"];
                    let y2 = sym["y2"];

                    graphics.lineStyle(2,0xbf4080,1);
                    graphics.beginFill(0xbf4080);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    symNom.x = (x1 + 0.02) * this.scale;
                    symNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(symNom);
                }
            }

            if (affgravity){
                let grav = this.gravity[0];
                if (typeof grav !== 'undefined'){
                    let centre = new PIXI.Graphics();
                    console.log(grav);
                    centre.beginFill(0xfaae0a);
                    centre.lineStyle(5, 0xfaae0a);
                    centre.drawRect(grav.x * this.scale,grav.y * this.scale,5,5);
                    graphics.addChild(centre);
                }
            }

            // console.log('LINES', lines, this.sideBisections);
        }
        return new PIXI.Sprite(graphics.generateCanvasTexture());
    }

    /**
     * Retourne le nom du polygone dans la langue demandée
     * @param {string} lang La langue désirée
     * @return {string} le nom du polygone
     */
    getName(lang) {
        // console.log(this.names);
        // console.log(lang);
        return this.names[lang];
    }

    /**
     * Retourne une couleur aléatoire parmis un set de couleurs prédéfinies
     * @return {int} la couleur en hexadécimal
     */
    getRandomColor() {
        let colors = [0xff9999, 0xff80ff, 0x99bbff, 0xadebad, 0xffdd99, 0xecb3ff, 0xb3ccff, 0xb3ffff, 0xb3b3ff, 0xc2efc2];
        let random = Math.floor(Math.random() * colors.length) + 1;
        return colors[random - 1];
    }

}
/**
 * @classdesc Object vecteur
 * @author Vincent Audergon
 * @version 1.0
 */
class Vector {

    /**
     * Constructeur de l'objet vector
     * @param {double} x La composante x du vecteur
     * @param {double} y La composante y du vecteur
     * @param {Point} offset L'origine du vecteur
     * @param {string} from Le nom du pont de départ
     * @param {string} to Le nom du point d'arrivé
     * @param {boolean} [calcDirection=true] Indique si la direction doit être calculée, vrai par défaut (facultatif)
     * @example
     *
     *      let v1 = new Vector(1,1,new Point(0,0),'A','B', true); //Avec calcul de la direction
     *      let v2 = new Vector(1,1,new Point(0,0),'A','B', false); //Sans calcul de la direction
     */
    constructor(x, y, offset, from, to, calcDirection = true) {
        /** @type {Point} l'origine du vecteur */
        this.offset = offset;
        /** @type {string} le nom du point de départ */
        this.from = from;
        /** @type {string} le nom du point d'arrivée */
        this.to = to;
        /** @type {Point} le sens du vecteur */
        this.way = new Point(x < 0 ? -1 : 1, y < 0 ? -1 : 1);
        /** @type {double} la composante x du vecteur */
        this.x = Math.abs(x);
        /** @type {double} la composante y du vecteur */
        this.y = Math.abs(y);
        /** @type {double} la norme du vecteur */
        this.norme = VectorUtils.calcNorme(this);
        /** @type {Vector} la direction du vecteur (0 à 180°) */
        this.direction = calcDirection ? VectorUtils.calcDirection(this) : NaN;
        /** @type {Vector} la direction complète du vecteur (0 à 360°) */
        this.fullDirection = calcDirection ? VectorUtils.calcFullDirection(this) : NaN;
    }

    /**
     * Retourne la composante x **signée**
     * @return {double} La composante x
     */
    getTrueX() {
        return this.x * this.way.x;
    }

    /**
     * Retourne la composante y **signée**
     * @return {double} La composante y
     */
    getTrueY() {
        return this.y * this.way.y;
    }

    /**
     * Effectue une addition sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    add(v2) {
        this.x += v2.x;
        this.y += v2.y;
    }

    /**
     * Effectue une soustraction sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    sub(v2) {
        this.x -= v2.x;
        this.y -= v2.y;
    }

    /**
     * Effectue une multiplication sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mult(v2) {
        this.x *= v2.x;
        this.y *= v2.y;
    }

    /**
     * Effectue une division sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    div(v2) {
        this.x /= v2.x;
        this.y /= v2.y;
    }

    /**
     * Effectue un modulo sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mod(v2) {
        this.x %= v2.x;
        this.y %= v2.y;
    }

    /**
     * Transforme le vecteur en chaîne de caractères
     * @return {string} la chaîne de caractères
     */
    toString() {
        return `[${this.x}, ${this.y}]`;
    }

    /**
     * Retourne un clone du vecteur
     * @return {Vector} le clone
     */
    clone() {
        return new Vector(this.x, this.y, this.offset, this.name);
    }

}
/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
class Cellule extends Asset {


    constructor(x, y, width, height, background, onclick) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.sprite = null;
        this.background = background;
        this.container = new PIXI.Container();
        this.container.width = 600;
        this.container.height = 600;
        this.onclick = onclick;
        this.type = '';
        this.init();
        this.tableIndex = {
            col: -1,
            row: -1
        }
    }

    setSprite(image) {
        this.sprite = {
            image: image,
            currentFrame: 0,
            speed: 100,
            lastUpdate: 0,
            sprite: null,
            size: 0,
            offset: 0
        };
        this.init();
    }

    getPixiChildren() {
        return [this.container];
    }

    init() {
        this.container.removeChildren();
        let backgrd = new PIXI.Sprite.fromImage(this.background.image.src);
        backgrd.anchor.x = 0;
        backgrd.anchor.y = 0;
        backgrd.x = this.x;
        backgrd.y = this.y;
        backgrd.width = this.width;
        backgrd.height = this.height;
        backgrd.interactive = true;
        backgrd.on('pointerdown', function () {
            if (this.onclick)
                this.onclick(this);
        }.bind(this));

        this.container.addChild(backgrd);

        if (this.containSprite()) {
            let image = this.sprite.image;
            let imageHeight = this.height < image.getHeight() ? this.height : image.getHeight();
            while (imageHeight / this.height < 0.6)
                imageHeight++;
            while (imageHeight / this.height > 0.6)
                imageHeight--;
            let imageWidth = (imageHeight / image.getHeight()) * image.getWidth() * image.getFrames();
            let sprite = new PIXI.Sprite.fromImage(image.image.src);
            sprite.anchor.x = 0;
            sprite.anchor.y = 0;
            sprite.x = this.x + (this.width - imageWidth / image.getFrames()) / 2;
            sprite.y = this.y + this.height * 0.2;
            sprite.width = imageWidth;
            sprite.height = imageHeight;
            if (image.getFrames() !== 1) {
                let mask = new PIXI.Graphics();
                mask.drawRect(this.x + (this.width - imageWidth / image.getFrames()) / 2, this.y, imageWidth / image.getFrames(), this.height);
                sprite.mask = mask;
            }
            this.sprite.sprite = sprite;
            this.sprite.size = imageWidth / image.getFrames();
            this.sprite.offset = this.x + (this.width - imageWidth / image.getFrames()) / 2;
            this.container.addChild(sprite);
        }

    }

    updateGif() {
        if (!this.containSprite())
            return;
        if (this.sprite.image.getFrames() === 1)
            return;
        let date = new Date().getTime();
        if (this.sprite.speed < (date - this.sprite.lastUpdate)) {
            this.sprite.lastUpdate = date;
            this.updateSprite(this.sprite);
        }
    }

    updateSprite(image) {
        image.currentFrame++;
        if (image.image.getFrames() < image.currentFrame + 1) {
            image.currentFrame = 1;
            image.sprite.x = image.offset;
        }
        image.sprite.x = image.sprite.x - image.size;
    }

    getY() {
        return this.y
    }

    getX() {
        return this.x
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.width;
    }

    containSprite() {
        return this.sprite;
    }

    getType() {
        return this.type;
    }

    setType(type) {
        this.type = type;
    }

    getHeight() {
        return this.getHeight();
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getTableIndex() {
        return this.tableIndex;
    }

    setTableIndex(col, row) {
        this.tableIndex.col = col;
        this.tableIndex.row = row;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        console.log(this.elements);

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
/**
 * @classdesc IHM de la création d'exercice
 * @author Sturzenegger Erwan
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */

class Create extends Interface {

    constructor(refGame) {

        super(refGame, "create");
        this.refGame = refGame;


        this.currentText = 'createModeIntro';
        this.cells = [];
        this.steps = [4, 8, 12];
        this.actualSize = 4;
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;

        this.screens = {
            title: {
                mainTitle: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'}),
                startButton: new Button(300, 400, '', 0x007bff, 0xFFFFFF, true)
            },
            levelResume: {
                resumeTitle: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'}),
                nextLevelbtn: new Button(500, 550, '', 0x007bff, 0xFFFFFF, true),
                stopLevelBtn: new Button(300, 550, '', 0x6c757d, 0xFFFFFF, true),
                deleteLevelBtn: new Button(100, 550, '', 0xdc3545, 0xFFFFFF, true),
            },
            game: {
                cellsContainer: new PIXI.Container()
            },
            submission: {
                submissionTitle: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                btnSubmit: new Button(300, 200, '', 0x28a745, 0xFFFFFF, false, 250),
                btnDelete: new Button(300, 300, '', 0xdc3545, 0xFFFFFF, false, 250),
                btnTest: new Button(300, 400, '', 0x007bff, 0xFFFFFF, false, 250)
            }
        };

        //Title setup
        this.screens.title.mainTitle.anchor.set(0.5);
        this.screens.title.mainTitle.x = 300;
        this.screens.title.mainTitle.y = 150;
        this.screens.title.startButton.setOnClick(this.startCreateMode.bind(this));

        //LevelResume setup
        this.screens.levelResume.resumeTitle.anchor.set(0.5);
        this.screens.levelResume.resumeTitle.x = 300;
        this.screens.levelResume.resumeTitle.y = 50;
        this.screens.levelResume.nextLevelbtn.setOnClick(this.handleSaveLevel.bind(this));
        this.screens.levelResume.stopLevelBtn.setOnClick(this.handleStopInsertion.bind(this));
        this.screens.levelResume.deleteLevelBtn.setOnClick(this.handleDeleteLevel.bind(this));

        //submission setup
        this.screens.submission.submissionTitle.anchor.set(0.5);
        this.screens.submission.submissionTitle.x = 300;
        this.screens.submission.submissionTitle.y = 100;
        this.screens.submission.btnDelete.setOnClick(this.handleDeleteExercice.bind(this));
        this.screens.submission.btnTest.setOnClick(this.handleTestExercice.bind(this));
        this.screens.submission.btnSubmit.setOnClick(this.handleSubmitExercice.bind(this));


        this.levels = {
            name: "",
            levels: []
        };
        this.currentLevel = {};

        setInterval(this.updateGif.bind(this), 1);

    }


    show(fromTrain = false) {
        if (!fromTrain) {
            this.currentLevel = {};
            this.levels = {
                name: "",
                levels: []
            };

            this.updateScreen('title', true);
            this.updateScreen('game', false);
            this.updateScreen('levelResume', false);
            this.updateScreen('submission', false);
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
            this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'sizeSlider', 'btnEnd', 'btnContinue');
            this.clear();
            for (let screen of Object.keys(this.screens))
                for (let comp of Object.keys(this.screens[screen]))
                    this.elements.push(this.screens[screen][comp]);
            this.refreshLang(this.refGame.global.resources.getLanguage());
            this.showTitle();
        }
        this.init();
    }


    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.screens.title.mainTitle.text = this.refGame.global.resources.getOtherText('welcome');
        this.screens.title.startButton.setText(this.refGame.global.resources.getOtherText('start'));
        this.screens.levelResume.resumeTitle.setText(this.refGame.global.resources.getOtherText('saveTitle'));
        this.screens.levelResume.deleteLevelBtn.setText(this.refGame.global.resources.getOtherText('deleteLevel'));
        this.screens.levelResume.nextLevelbtn.setText(this.refGame.global.resources.getOtherText('nextLevel'));
        this.screens.levelResume.stopLevelBtn.setText(this.refGame.global.resources.getOtherText('stopInsertion'));
        this.screens.submission.btnDelete.setText(this.refGame.global.resources.getOtherText('deleteExercice'));
        this.screens.submission.btnSubmit.setText(this.refGame.global.resources.getOtherText('submitExercice'));
        this.screens.submission.btnTest.setText(this.refGame.global.resources.getOtherText('testExercice'));
        this.screens.submission.submissionTitle.text = this.refGame.global.resources.getTutorialText('confirmExercice');
    }

    showTitle() {
        this.currentText = 'createModeIntro';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    updateScreen(screen, visble) {
        for (let component of Object.keys(this.screens[screen])) {
            if (this.screens[screen][component] instanceof Asset)
                this.screens[screen][component].setVisible(visble);
            else if (this.screens[screen][component].hasOwnProperty('visible'))
                this.screens[screen][component].visible = visble;
        }
    }

    startCreateMode() {
        this.levels = {
            name: "",
            levels: []
        };
        this.newGame();
    }

    showLevelResume() {
        this.prepareLevel();
        this.generateCells(400, 100, 100);
        this.loadCurrentLevel();
        this.updateScreen('levelResume', true);
        this.updateScreen('game', true);
        this.refGame.global.util.hideTutorialInputs('next');
    }

    handleSaveLevel() {
        this.levels.levels.push(this.currentLevel);
        this.currentLevel = {};
        this.newGame();
    }

    handleDeleteLevel() {
        this.currentLevel = {};
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('confirmDeletion'),
            text: this.refGame.global.resources.getOtherText('confirmDeletionText'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: this.refGame.global.resources.getOtherText('yes'),
            cancelButtonText: this.refGame.global.resources.getOtherText('no')
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('deleted'),
                    this.refGame.global.resources.getOtherText('deletedLevelText'),
                    'success'
                );
                this.newGame();
            }
        });
    }

    handleStopInsertion() {
        this.levels.levels.push(this.currentLevel);
        this.currentLevel = {};
        this.currentText = 'chooseOption';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.updateScreen('levelResume', false);
        this.updateScreen('game', false);
        this.updateScreen('submission', true);
    }

    prepareLevel() {
        let level = {
            size: this.actualSize,
            cells: {
                start: {},
                stop: {},
                walls: [],
                mandatories: []
            }
        };
        for (let col of this.cells)
            for (let row of col) {
                if (!row.getType())
                    continue;
                switch (row.getType()) {
                    case 'start':
                        level.cells.start.x = row.getTableIndex().col;
                        level.cells.start.y = row.getTableIndex().row;
                        break;
                    case 'end':
                        level.cells.stop.x = row.getTableIndex().col;
                        level.cells.stop.y = row.getTableIndex().row;
                        break;
                    case 'wall':
                        level.cells.walls.push({
                            x: row.getTableIndex().col,
                            y: row.getTableIndex().row
                        });
                        break;
                    case 'coin':
                        level.cells.mandatories.push({
                            x: row.getTableIndex().col,
                            y: row.getTableIndex().row
                        });
                        break;
                }
            }
        this.currentLevel = level;
    }

    loadCurrentLevel() {
        this.cells[this.currentLevel.cells.start.x][this.currentLevel.cells.start.y].setType('start');
        this.cells[this.currentLevel.cells.start.x][this.currentLevel.cells.start.y].setSprite(this.refGame.global.resources.getImage('hero'));

        this.cells[this.currentLevel.cells.stop.x][this.currentLevel.cells.stop.y].setType('end');
        this.cells[this.currentLevel.cells.stop.x][this.currentLevel.cells.stop.y].setSprite(this.refGame.global.resources.getImage('chest'));

        for (let wall of this.currentLevel.cells.walls) {
            this.cells[wall.x][wall.y].setType('wall');
            this.cells[wall.x][wall.y].setSprite(this.refGame.global.resources.getImage('bat'));
        }

        for (let coin of this.currentLevel.cells.mandatories) {
            this.cells[coin.x][coin.y].setType('coin');
            this.cells[coin.x][coin.y].setSprite(this.refGame.global.resources.getImage('coin'));
        }
    }

    newGame() {
        this.updateScreen('title', false);
        this.updateScreen('levelResume', false);
        this.updateScreen('game', true);
        this.currentText = 'startCreate';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.refGame.global.util.showTutorialInputs('next');
        this.cells = [];
        this.currentLevel = {};
        this.steps = [4, 8, 12];
        this.actualSize = this.steps[this.refGame.global.resources.getDegre() - 1];
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;
        this.generateCells();
    }

    generateCells(forcedSize = 600, forcedX = 0, forcedY = 0) {
        var sideX = forcedSize / this.actualSize;
        var sideY = forcedSize / this.actualSize;
        var cells = [];
        this.screens.game.cellsContainer.removeChildren();
        var x = forcedX, y = forcedY;
        for (var i = 0; i < this.actualSize; i++) {
            y = forcedY;
            cells[i] = [];
            for (var j = 0; j < this.actualSize; j++) {
                var curCell = new Cellule(x, y, sideX, sideY, this.refGame.global.resources.getImage('cell'), this.handleCellClick.bind(this));
                curCell.setTableIndex(i, j);
                cells[i].push(curCell);
                for (let el of curCell.getPixiChildren())
                    this.screens.game.cellsContainer.addChild(el);
                y += sideY;
            }
            x += sideX;
        }
        this.cells = cells;
    }

    updateGif() {
        for (let col of this.cells)
            for (let cell of col)
                cell.updateGif();
    }

    handleCellClick(cell) {
        if (cell.containSprite())
            return;
        if (!this.isStartDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('hero'));
            cell.setType('start');
            this.isStartDrawn = true;
            this.currentText = 'end';
        } else if (!this.isStopDrawn) {
            if (this.testForStopPoint(cell)) {
                this.isStopDrawn = true;
                cell.setSprite(this.refGame.global.resources.getImage('chest'));
                cell.setType('end');
                this.currentText = 'coin';
            } else {
                this.refGame.global.util.showAlert(
                    'warning',
                    this.refGame.global.resources.getOtherText('tooclose'),
                    this.refGame.global.resources.getOtherText('error'),
                    undefined,
                    undefined);
            }
        } else if (!this.areMandatoryDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('coin'));
            cell.setType('coin');
            this.coinCount++;
        } else if (!this.areWallsDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('bat'));
            cell.setType('wall');
        }
        if (!this.testForEndReachable() || !this.testForCoinsReachables()) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('notfeasable'),
                this.refGame.global.resources.getOtherText('error'),
                undefined,
                this.newGame.bind(this));
        }

        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    handleNext() {
        if (this.isStartDrawn && this.isStopDrawn) {
            if (!this.areMandatoryDrawn) {
                this.areMandatoryDrawn = true;
                this.currentText = 'wall';
            } else if (!this.areWallsDrawn) {
                this.areWallsDrawn = true;
                this.updateScreen('game', false);
                this.showLevelResume();
                this.currentText = 'chooseOption';
            }
        }
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    testForStopPoint(cell) {
        let indexes = cell.getTableIndex();

        if (indexes.col > 0 && this.cells[indexes.col - 1][indexes.row].getType() === 'start')
            return false;
        if (indexes.row > 0 && this.cells[indexes.col][indexes.row - 1].getType() === 'start')
            return false;
        if (indexes.row < this.cells[indexes.col].length - 1 && this.cells[indexes.col][indexes.row + 1].getType() === 'start')
            return false;
        if (indexes.col < this.cells.length - 1 && this.cells[indexes.col + 1][indexes.row].getType() === 'start')
            return false;

        return true;
    }

    testForEndReachable() {
        this.endPos = null;
        let startCell = null;
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'start':
                        startCell = cell;
                        break;
                }
            }
        }
        this.testedCell = [];
        this.findPathToEnd(startCell);
        return !(this.endPos === null && this.isStartDrawn && this.isStopDrawn);
    }

    findPathToEnd(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'end')
            this.endPos = cell.getTableIndex();

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToEnd(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToEnd(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToEnd(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToEnd(this.cells[indexes.col + 1][indexes.row]);
    }

    testForCoinsReachables() {
        let coinCells = [];
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'coin':
                        coinCells.push(cell);
                        break;
                }
            }
        }
        let foundCount = 0;
        for (let coinCell of coinCells) {
            this.testedCell = [];
            this.pathToHeroFound = false;
            this.findPathToHero(coinCell);
            if (this.pathToHeroFound)
                foundCount++;
        }
        this.testedCell = [];
        return foundCount === coinCells.length;
    }

    findPathToHero(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'start')
            this.pathToHeroFound = true;

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToHero(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToHero(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToHero(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToHero(this.cells[indexes.col + 1][indexes.row]);
    }

    handleDeleteExercice() {
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('confirmDeletion'),
            text: this.refGame.global.resources.getOtherText('confirmDeletionText'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33d33',
            confirmButtonText: this.refGame.global.resources.getOtherText('yes'),
            cancelButtonText: this.refGame.global.resources.getOtherText('no')
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('deleted'),
                    this.refGame.global.resources.getOtherText('deletedExerciceText'),
                    'success'
                );
                this.show();
            }
        });
    }

    handleTestExercice() {
        this.refGame.trainMode.init(false);
        this.refGame.trainMode.show(this.levels);
    }

    handleSubmitExercice() {
        this.refGame.global.resources.saveExercice(JSON.stringify(this.levels), function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );
            this.show();
        }.bind(this));
    }
}
/**
 * @classdesc IHM de la création d'exercice
 * @author Sturzenegger Erwan
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */

class Explore extends Interface {

    constructor(refGame) {

        super(refGame, "explore");
        this.refGame = refGame;


        this.currentText = 'start';
        this.cells = [];
        this.steps = [4, 6, 8, 10, 12];
        this.actualSize = 4;
        this.spriteSize = this.steps[0] / this.actualSize;
        this.squareSide = 600 / this.actualSize;
        this.lastDrawCell = null;
        this.coinCount = 0;
        this.coinRecovered = 0;
        this.testedCell = [];
        this.endPos = -1;
        this.pathToHeroFound = false;
        this.moveCount = 0;
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;
        this.isDrawing = false;

        this.cellsContainer = new PIXI.Container();

        this.elements.push(this.cellsContainer);
        setInterval(this.updateGif.bind(this), 1);

    }


    show() {
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('sizeSlider'), ['input', 'touchstart', 'touchmove'], this._onResize.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousedown', 'touchstart'], this._eventStart.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mouseup', 'mouseout', 'touchend', 'touchleave'], this._eventStop.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousemove', 'touchmove'], this._eventMove.bind(this));
        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'sizeSlider', 'btnEnd', 'btnContinue');
        this.refGame.global.util.showTutorialInputs('next', 'btnReset', 'sizeSlider');
        this.clear();
        this.newGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }


    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    newGame() {
        this.currentText = 'start';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.cells = [];
        this.steps = [4, 6, 8, 10, 12];
        this.actualSize = 4;
        this.lastDrawCell = null;
        this.coinCount = 0;
        this.coinRecovered = 0;
        this.testedCell = [];
        this.endPos = -1;
        this.pathToHeroFound = false;
        this.moveCount = 0;
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;
        this.isDrawing = false;
        this.generateCells();
    }

    _eventStart(e) {
        e.preventDefault();
        this.isDrawing = true;
    }

    _eventMove(e) {
        e.preventDefault();
        if (this.isStartDrawn && this.isStopDrawn && this.areMandatoryDrawn && this.areWallsDrawn && this.isDrawing)
            this.drawPath(e);
    }

    _eventStop(e) {
        e.preventDefault();
        this.isDrawing = false;
    }

    _onResize(e) {
        this.actualSize = parseInt(this.steps[e.target.value]);
        this.generateCells();
    };

    generateCells() {
        var sideX = 600 / this.actualSize;
        var sideY = 600 / this.actualSize;
        var cells = [];
        this.cellsContainer.removeChildren();
        var x = 0, y = 0;
        for (var i = 0; i < this.actualSize; i++) {
            y = 0;
            cells[i] = [];
            for (var j = 0; j < this.actualSize; j++) {
                var curCell = new Cellule(x, y, sideX, sideY, this.refGame.global.resources.getImage('cell'), this.handleCellClick.bind(this));
                curCell.setTableIndex(i, j);
                cells[i].push(curCell);
                for (let el of curCell.getPixiChildren())
                    this.cellsContainer.addChild(el);
                y += sideY;
            }
            x += sideX;
        }
        this.cells = cells;
    }

    updateGif() {
        for (let col of this.cells)
            for (let cell of col)
                cell.updateGif();
    }

    handleCellClick(cell) {
        if (cell.containSprite())
            return;
        if (!this.isStartDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('hero'));
            cell.setType('start');
            this.isStartDrawn = true;
            this.currentText = 'end';
        } else if (!this.isStopDrawn) {
            if (this.testForStopPoint(cell)) {
                this.isStopDrawn = true;
                cell.setSprite(this.refGame.global.resources.getImage('chest'));
                cell.setType('end');
                this.currentText = 'coin';
            } else {
                this.refGame.global.util.showAlert(
                    'warning',
                    this.refGame.global.resources.getOtherText('tooclose'),
                    this.refGame.global.resources.getOtherText('error'),
                    undefined,
                    undefined);
            }
        } else if (!this.areMandatoryDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('coin'));
            cell.setType('coin');
            this.coinCount++;
        } else if (!this.areWallsDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('bat'));
            cell.setType('wall');
        }
        if (!this.testForEndReachable() || !this.testForCoinsReachables()) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('notfeasable'),
                this.refGame.global.resources.getOtherText('error'),
                undefined,
                this.newGame.bind(this));
        }

        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    handleNext() {
        if (this.isStartDrawn && this.isStopDrawn) {
            if (!this.areMandatoryDrawn) {
                this.areMandatoryDrawn = true;
                this.currentText = 'wall';
            } else if (!this.areWallsDrawn) {
                this.areWallsDrawn = true;
                this.currentText = 'gameExplore';
            }
        }
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    testForStopPoint(cell) {
        let indexes = cell.getTableIndex();

        if (indexes.col > 0 && this.cells[indexes.col - 1][indexes.row].getType() === 'start')
            return false;
        if (indexes.row > 0 && this.cells[indexes.col][indexes.row - 1].getType() === 'start')
            return false;
        if (indexes.row < this.cells[indexes.col].length - 1 && this.cells[indexes.col][indexes.row + 1].getType() === 'start')
            return false;
        if (indexes.col < this.cells.length - 1 && this.cells[indexes.col + 1][indexes.row].getType() === 'start')
            return false;

        return true;
    }

    testForEndReachable() {
        this.endPos = null;
        let startCell = null;
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'start':
                        startCell = cell;
                        break;
                }
            }
        }
        this.testedCell = [];
        this.findPathToEnd(startCell);
        return !(this.endPos === null && this.isStartDrawn && this.isStopDrawn);
    }

    findPathToEnd(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'end')
            this.endPos = cell.getTableIndex();

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToEnd(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToEnd(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToEnd(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToEnd(this.cells[indexes.col + 1][indexes.row]);
    }

    testForCoinsReachables() {
        let coinCells = [];
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'coin':
                        coinCells.push(cell);
                        break;
                }
            }
        }
        let foundCount = 0;
        for (let coinCell of coinCells) {
            this.testedCell = [];
            this.pathToHeroFound = false;
            this.findPathToHero(coinCell);
            if (this.pathToHeroFound)
                foundCount++;
        }
        this.testedCell = [];
        return foundCount === coinCells.length;
    }

    findPathToHero(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'start')
            this.pathToHeroFound = true;

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToHero(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToHero(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToHero(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToHero(this.cells[indexes.col + 1][indexes.row]);
    }

    drawPath(e) {
        let cell = this.findCellForEvent(e);
        if (!this.lastDrawCell && cell.getType() !== 'start')
            return;
        if (!this.lastDrawCell && cell.getType() === 'start') {
            this.lastDrawCell = cell;
            return;
        }
        if (cell.getType() && cell.getType() !== 'coin' && cell.getType() !== 'end')
            return;

        let currentIndexes = cell.getTableIndex();

        if (Math.abs(currentIndexes.col - this.lastDrawCell.getTableIndex().col) + Math.abs(currentIndexes.row - this.lastDrawCell.getTableIndex().row) > 1)
            return;
        let direction = '';
        if (currentIndexes.col < this.lastDrawCell.getTableIndex().col)
            direction = 'left';
        else if (currentIndexes.col > this.lastDrawCell.getTableIndex().col)
            direction = 'right';
        else if (currentIndexes.row < this.lastDrawCell.getTableIndex().row)
            direction = 'up';
        else if (currentIndexes.row > this.lastDrawCell.getTableIndex().row)
            direction = 'down';
        else
            return;
        if (this.lastDrawCell.getType() !== 'start') {
            this.lastDrawCell.setSprite(this.refGame.global.resources.getImage(direction));
            this.lastDrawCell.setType(direction);
        }
        this.lastDrawCell = cell;

        if (cell.getType() === 'coin')
            this.coinRecovered++;

        if (cell.getType() !== 'end') {
            cell.setSprite(this.refGame.global.resources.getImage('current_' + direction));
            cell.setType('current_' + direction);
            this.moveCount++;
        } else if (cell.getType() === 'end') {
            if (this.coinCount === this.coinRecovered)
                this.refGame.global.util.showAlert(
                    'success',
                    this.refGame.global.resources.getOtherText('victory', {count: this.moveCount + 2}),
                    this.refGame.global.resources.getOtherText('success'), undefined,
                    this.newGame.bind(this)
                );
            else
                this.refGame.global.util.showAlert(
                    'error',
                    this.refGame.global.resources.getOtherText('defeat'),
                    this.refGame.global.resources.getOtherText('failure'), undefined, function () {
                        this.newGame();
                    }.bind(this));

        }
        if (this.checkDefeat(cell)) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('blocked'),
                this.refGame.global.resources.getOtherText('failure'),
                undefined,
                this.newGame.bind(this));
        }
    }

    findCellForEvent(e) {
        this.refGame.global.util.globalizeEvent(e);
        let cellSize = 600 / this.actualSize;
        let col = Math.floor((e.clientX - this.refGame.global.canvas.OFFSETLEFT) / cellSize);
        let row = Math.floor((e.clientY - this.refGame.global.canvas.OFFSETTOP) / cellSize);
        if (col >= this.actualSize)
            col = this.actualSize - 1;
        if (row >= this.actualSize)
            row = this.actualSize - 1;
        if (col < 0)
            col = 0;
        if (row < 0)
            row = 0;
        return this.cells[col][row];
    }

    checkDefeat(cell) {
        let indexes = cell.getTableIndex();
        return !((indexes.col > 0 && (!this.cells[indexes.col - 1][indexes.row].getType() || this.cells[indexes.col - 1][indexes.row].getType() === 'coin'))
            || (indexes.row > 0 && (!this.cells[indexes.col][indexes.row - 1].getType() || this.cells[indexes.col][indexes.row - 1].getType() === 'coin'))
            || (indexes.row < this.cells[indexes.col].length - 1 && (!this.cells[indexes.col][indexes.row + 1].getType() || this.cells[indexes.col][indexes.row + 1].getType() === 'coin'))
            || (indexes.col < this.cells.length - 1 && (!this.cells[indexes.col + 1][indexes.row].getType() || this.cells[indexes.col + 1][indexes.row].getType() === 'coin')));
    }
}
/**
 * @classdesc IHM de la création d'exercice
 * @author Sturzenegger Erwan
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */

class Train extends Interface {
    constructor(refGame) {

        super(refGame, "train");
        this.refGame = refGame;

        this.tryMode = false;

        this.currentText = 'start';
        this.cells = [];
        this.steps = [4, 6, 8, 10, 12];
        this.actualSize = 4;
        this.lastDrawCell = null;
        this.coinCount = 0;
        this.coinRecovered = 0;
        this.moveCount = 0;
        this.isDrawing = false;
        this.goodLevels = 0;
        this.currentLevel = 0;
        this.exercice = null;
        this.evaluate = false;
        this.cellsContainer = new PIXI.Container();
        this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.startButton.setOnClick(this.newGame.bind(this));
        this.backToCreateModeBtn = new Button(100, 575, '', 0x007bff, 0xFFFFFF, false, 200);
        this.backToCreateModeBtn.setOnClick(this.backToCreateMode.bind(this));
        this.title.x = 300;
        this.title.y = 150;
        this.title.anchor.set(0.5);
        this.elements.push(this.cellsContainer);
        this.elements.push(this.title);
        this.elements.push(this.startButton);
        this.elements.push(this.backToCreateModeBtn);
        setInterval(this.updateGif.bind(this), 1);

    }


    show(evaluate, forcedExerice = null) {
        if (forcedExerice)
            this.tryMode = true;
        else
            this.tryMode = false;
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousedown', 'touchstart'], this._eventStart.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mouseup', 'mouseout', 'touchend', 'touchleave'], this._eventStop.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousemove', 'touchmove'], this._eventMove.bind(this));
        this.evaluate = evaluate;
        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'sizeSlider', 'btnEnd', 'btnContinue');
        if (this.tryMode) {
            this.backToCreateModeBtn.setVisible(true);
            this.newGame(forcedExerice);
        } else {
            this.showTitle();
            this.backToCreateModeBtn.setVisible(false);
        }
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
        this.setupSignalement();
    }

    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        if (this.evaluate)
            this.title.text = this.refGame.global.resources.getOtherText('evalModeTitle');
        else
            this.title.text = this.refGame.global.resources.getOtherText('trainModeTitle');
        this.startButton.setText(this.refGame.global.resources.getOtherText('startGame'));
        this.backToCreateModeBtn.setText(this.refGame.global.resources.getOtherText('backToCreateMode'));
    }

    showTitle() {
        this.cellsContainer.visible = false;
        this.title.visible = true;
        this.startButton.setVisible(true);
    }

    newGame(forcedExerice = null) {
        if (forcedExerice)
            this.exercice = forcedExerice;
        else {
            this.exercice = this.refGame.global.resources.getExercice();
            this.currentExerciceId = this.exercice.id;
            this.exercice = JSON.parse(this.exercice.exercice);
            this.refGame.inputs.signal.style.display = 'block';
        }
        if (this.evaluate)
            this.refGame.global.statistics.addStats(2);

        this.title.visible = false;
        this.startButton.setVisible(false);
        this.cellsContainer.visible = true;

        this.currentLevel = 0;
        this.goodLevels = 0;
        this.nextLevel();
    }

    nextLevel() {
        if (this.currentLevel >= this.exercice.levels.length) {
            this.showEndExercice();
        } else {
            this.currentText = 'gameTrain';
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
            this.cells = [];
            this.steps = [4, 6, 8, 10, 12];
            this.actualSize = this.exercice.levels[this.currentLevel].size;
            this.lastDrawCell = null;
            this.coinCount = 0;
            this.coinRecovered = 0;
            this.moveCount = 0;
            this.isDrawing = false;
            this.generateCells();
            this.fillExercice(this.exercice.levels[this.currentLevel]);
            this.currentLevel++;
        }
    }

    showEndExercice() {

        if (!this.tryMode) {
            if (this.evaluate) {
                let mark = 0;
                let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
                let perc = (this.goodLevels / this.exercice.levels.length) * 100;
                for (let b of barem) {
                    if (b.min <= perc && b.max >= perc) {
                        mark = b.note;
                    }
                }
                this.refGame.global.statistics.updateStats(mark);
            } else {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('exerciceEnd'),
                    this.refGame.global.resources.getOtherText('exerciceEndText', {
                        count: this.goodLevels,
                        tot: this.exercice.levels.length
                    }),
                    'success'
                );
            }
            this.showTitle();
        } else
            this.backToCreateMode();
    }

    _eventStart(e) {
        e.preventDefault();
        this.isDrawing = true;
    }

    _eventMove(e) {
        e.preventDefault();
        if (this.isDrawing)
            this.drawPath(e);
    }

    _eventStop(e) {
        e.preventDefault();
        this.isDrawing = false;
    }


    fillExercice(exercice) {
        this.cells[exercice.cells.start.x][exercice.cells.start.y].setType('start');
        this.cells[exercice.cells.start.x][exercice.cells.start.y].setSprite(this.refGame.global.resources.getImage('hero'));

        this.cells[exercice.cells.stop.x][exercice.cells.stop.y].setType('end');
        this.cells[exercice.cells.stop.x][exercice.cells.stop.y].setSprite(this.refGame.global.resources.getImage('chest'));

        for (let wall of exercice.cells.walls) {
            this.cells[wall.x][wall.y].setType('wall');
            this.cells[wall.x][wall.y].setSprite(this.refGame.global.resources.getImage('bat'));
        }

        for (let coin of exercice.cells.mandatories) {
            this.cells[coin.x][coin.y].setType('coin');
            this.cells[coin.x][coin.y].setSprite(this.refGame.global.resources.getImage('coin'));
            this.coinCount++;
        }

    }

    generateCells() {
        var sideX = (this.tryMode ? 550 : 600) / this.actualSize;
        var sideY = (this.tryMode ? 550 : 600) / this.actualSize;
        var cells = [];
        this.cellsContainer.removeChildren();
        var x = 0, y = 0;
        for (var i = 0; i < this.actualSize; i++) {
            y = 0;
            cells[i] = [];
            for (var j = 0; j < this.actualSize; j++) {
                var curCell = new Cellule(x, y, sideX, sideY, this.refGame.global.resources.getImage('cell'), undefined);
                curCell.setTableIndex(i, j);
                cells[i].push(curCell);
                for (let el of curCell.getPixiChildren())
                    this.cellsContainer.addChild(el);
                y += sideY;
            }
            x += sideX;
        }
        this.cells = cells;
    }

    updateGif() {
        for (let col of this.cells)
            for (let cell of col)
                cell.updateGif();
    }


    drawPath(e) {
        let cell = this.findCellForEvent(e);
        if (!this.lastDrawCell && cell.getType() !== 'start')
            return;
        if (!this.lastDrawCell && cell.getType() === 'start') {
            this.lastDrawCell = cell;
            return;
        }
        if (cell.getType() && cell.getType() !== 'coin' && cell.getType() !== 'end')
            return;

        let currentIndexes = cell.getTableIndex();

        if (Math.abs(currentIndexes.col - this.lastDrawCell.getTableIndex().col) + Math.abs(currentIndexes.row - this.lastDrawCell.getTableIndex().row) > 1)
            return;
        let direction = '';
        if (currentIndexes.col < this.lastDrawCell.getTableIndex().col)
            direction = 'left';
        else if (currentIndexes.col > this.lastDrawCell.getTableIndex().col)
            direction = 'right';
        else if (currentIndexes.row < this.lastDrawCell.getTableIndex().row)
            direction = 'up';
        else if (currentIndexes.row > this.lastDrawCell.getTableIndex().row)
            direction = 'down';
        else
            return;
        if (this.lastDrawCell.getType() !== 'start') {
            this.lastDrawCell.setSprite(this.refGame.global.resources.getImage(direction));
            this.lastDrawCell.setType(direction);
        }
        this.lastDrawCell = cell;

        if (cell.getType() === 'coin')
            this.coinRecovered++;
        if (cell.getType() !== 'end') {
            cell.setSprite(this.refGame.global.resources.getImage('current_' + direction));
            cell.setType('current_' + direction);
            this.moveCount++;
        } else if (cell.getType() === 'end') {
            if (this.coinCount === this.coinRecovered) {
                this.goodLevels++;
                this.refGame.global.util.showAlert(
                    'success',
                    this.refGame.global.resources.getOtherText('victory', {count: this.moveCount + 2}),
                    this.refGame.global.resources.getOtherText('success'), undefined,
                    this.nextLevel.bind(this)
                );
            } else
                this.refGame.global.util.showAlert(
                    'error',
                    this.refGame.global.resources.getOtherText('defeat'),
                    this.refGame.global.resources.getOtherText('failure'), undefined, this.nextLevel.bind(this));

        }
        if (this.checkDefeat(cell)) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('blocked'),
                this.refGame.global.resources.getOtherText('failure'),
                undefined,
                this.nextLevel.bind(this));
        }
    }

    findCellForEvent(e) {
        this.refGame.global.util.globalizeEvent(e);
        let cellSize = (this.tryMode ? 550 : 600) / this.actualSize;
        let col = Math.floor((e.clientX - this.refGame.global.canvas.OFFSETLEFT) / cellSize);
        let row = Math.floor((e.clientY - this.refGame.global.canvas.OFFSETTOP) / cellSize);
        if (col >= this.actualSize)
            col = this.actualSize - 1;
        if (row >= this.actualSize)
            row = this.actualSize - 1;
        if (col < 0)
            col = 0;
        if (row < 0)
            row = 0;
        return this.cells[col][row];
    }

    checkDefeat(cell) {
        let indexes = cell.getTableIndex();
        return !((indexes.col > 0 && (!this.cells[indexes.col - 1][indexes.row].getType() || this.cells[indexes.col - 1][indexes.row].getType() === 'coin'))
            || (indexes.row > 0 && (!this.cells[indexes.col][indexes.row - 1].getType() || this.cells[indexes.col][indexes.row - 1].getType() === 'coin'))
            || (indexes.row < this.cells[indexes.col].length - 1 && (!this.cells[indexes.col][indexes.row + 1].getType() || this.cells[indexes.col][indexes.row + 1].getType() === 'coin'))
            || (indexes.col < this.cells.length - 1 && (!this.cells[indexes.col + 1][indexes.row].getType() || this.cells[indexes.col + 1][indexes.row].getType() === 'coin')));
    }

    backToCreateMode() {
        this.refGame.createMode.show(true);
    }

    setupSignalement() {
        this.refGame.inputs.signal.onclick = function () {
            Swal.fire({
                title: this.refGame.global.resources.getOtherText('signalementTitle'),
                cancelButtonText: this.refGame.global.resources.getOtherText('signalementCancel'),
                showCancelButton: true,
                html:
                    '<label>' + this.refGame.global.resources.getOtherText('signalementReason') + '</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                    '<label>' + this.refGame.global.resources.getOtherText('signalementPassword') + '</label><input type="password" id="swal-password" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-reason').val(),
                            $('#swal-password').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-reason').focus()
                }
            }).then(function (result) {
                if (result && result.value) {
                    this.refGame.global.resources.signaler(result.value[0], result.value[1], this.currentExerciceId, function (res) {
                        Swal.fire(res.message, '', res.type);
                        if (res.type === 'success') {
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);
    }
}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }
}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class CreateMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('create');
        this.refGame = refGame;
        this.setInterfaces({ create: new Create(refGame)})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(fromTrain = false) {
        this.interfaces.create.show(fromTrain);
    }

}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class ExploreMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('explore');
        this.refGame = refGame;
        this.drawmode = false;
        this.setInterfaces({ explore: new Explore(refGame)})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show() {
        this.interfaces.explore.show();
    }

}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class TrainMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({ train: new Train(refGame)})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init(evaluate) {
        this.evaluate=evaluate;
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(forcedExercice = null) {
        this.interfaces.train.show(this.evaluate,forcedExercice);
    }
}
/**
 * @classdesc Classe principale du jeu
 * @author Erwan Sturzenegger
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;

        this.scenes = {
            explore: new PIXI.Container(),
            train: new PIXI.Container(),
            create: new PIXI.Container()
        };

        this.inputs = {
            signal: document.getElementById('signal')
        };

        $('#onclick_gamemode_play').hide();

        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.oldGamemode = undefined;
   }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        for (let input in this.inputs) {
            this.inputs[input].style.display = 'none';
        }
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }


    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic) {

    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        this.global.listenerManager.clearAllInputListener();
        this.inputs = {
            signal: document.getElementById('signal')
        };

        let gamemode = this.global.util.getGamemode();
        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                 this.trainMode.init(true);
                 this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                // this.playMode.init();
                // this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
