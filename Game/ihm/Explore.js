/**
 * @classdesc IHM de la création d'exercice
 * @author Sturzenegger Erwan
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */

class Explore extends Interface {

    constructor(refGame) {

        super(refGame, "explore");
        this.refGame = refGame;


        this.currentText = 'start';
        this.cells = [];
        this.steps = [4, 6, 8, 10, 12];
        this.actualSize = 4;
        this.spriteSize = this.steps[0] / this.actualSize;
        this.squareSide = 600 / this.actualSize;
        this.lastDrawCell = null;
        this.coinCount = 0;
        this.coinRecovered = 0;
        this.testedCell = [];
        this.endPos = -1;
        this.pathToHeroFound = false;
        this.moveCount = 0;
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;
        this.isDrawing = false;

        this.cellsContainer = new PIXI.Container();

        this.elements.push(this.cellsContainer);
        setInterval(this.updateGif.bind(this), 1);

    }


    show() {
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('sizeSlider'), ['input', 'touchstart', 'touchmove'], this._onResize.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousedown', 'touchstart'], this._eventStart.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mouseup', 'mouseout', 'touchend', 'touchleave'], this._eventStop.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousemove', 'touchmove'], this._eventMove.bind(this));
        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'sizeSlider', 'btnEnd', 'btnContinue');
        this.refGame.global.util.showTutorialInputs('next', 'btnReset', 'sizeSlider');
        this.clear();
        this.newGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }


    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    newGame() {
        this.currentText = 'start';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.cells = [];
        this.steps = [4, 6, 8, 10, 12];
        this.actualSize = 4;
        this.lastDrawCell = null;
        this.coinCount = 0;
        this.coinRecovered = 0;
        this.testedCell = [];
        this.endPos = -1;
        this.pathToHeroFound = false;
        this.moveCount = 0;
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;
        this.isDrawing = false;
        this.generateCells();
    }

    _eventStart(e) {
        e.preventDefault();
        this.isDrawing = true;
    }

    _eventMove(e) {
        e.preventDefault();
        if (this.isStartDrawn && this.isStopDrawn && this.areMandatoryDrawn && this.areWallsDrawn && this.isDrawing)
            this.drawPath(e);
    }

    _eventStop(e) {
        e.preventDefault();
        this.isDrawing = false;
    }

    _onResize(e) {
        this.actualSize = parseInt(this.steps[e.target.value]);
        this.generateCells();
    };

    generateCells() {
        var sideX = 600 / this.actualSize;
        var sideY = 600 / this.actualSize;
        var cells = [];
        this.cellsContainer.removeChildren();
        var x = 0, y = 0;
        for (var i = 0; i < this.actualSize; i++) {
            y = 0;
            cells[i] = [];
            for (var j = 0; j < this.actualSize; j++) {
                var curCell = new Cellule(x, y, sideX, sideY, this.refGame.global.resources.getImage('cell'), this.handleCellClick.bind(this));
                curCell.setTableIndex(i, j);
                cells[i].push(curCell);
                for (let el of curCell.getPixiChildren())
                    this.cellsContainer.addChild(el);
                y += sideY;
            }
            x += sideX;
        }
        this.cells = cells;
    }

    updateGif() {
        for (let col of this.cells)
            for (let cell of col)
                cell.updateGif();
    }

    handleCellClick(cell) {
        if (cell.containSprite())
            return;
        if (!this.isStartDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('hero'));
            cell.setType('start');
            this.isStartDrawn = true;
            this.currentText = 'end';
        } else if (!this.isStopDrawn) {
            if (this.testForStopPoint(cell)) {
                this.isStopDrawn = true;
                cell.setSprite(this.refGame.global.resources.getImage('chest'));
                cell.setType('end');
                this.currentText = 'coin';
            } else {
                this.refGame.global.util.showAlert(
                    'warning',
                    this.refGame.global.resources.getOtherText('tooclose'),
                    this.refGame.global.resources.getOtherText('error'),
                    undefined,
                    undefined);
            }
        } else if (!this.areMandatoryDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('coin'));
            cell.setType('coin');
            this.coinCount++;
        } else if (!this.areWallsDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('bat'));
            cell.setType('wall');
        }
        if (!this.testForEndReachable() || !this.testForCoinsReachables()) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('notfeasable'),
                this.refGame.global.resources.getOtherText('error'),
                undefined,
                this.newGame.bind(this));
        }

        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    handleNext() {
        if (this.isStartDrawn && this.isStopDrawn) {
            if (!this.areMandatoryDrawn) {
                this.areMandatoryDrawn = true;
                this.currentText = 'wall';
            } else if (!this.areWallsDrawn) {
                this.areWallsDrawn = true;
                this.currentText = 'gameExplore';
            }
        }
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    testForStopPoint(cell) {
        let indexes = cell.getTableIndex();

        if (indexes.col > 0 && this.cells[indexes.col - 1][indexes.row].getType() === 'start')
            return false;
        if (indexes.row > 0 && this.cells[indexes.col][indexes.row - 1].getType() === 'start')
            return false;
        if (indexes.row < this.cells[indexes.col].length - 1 && this.cells[indexes.col][indexes.row + 1].getType() === 'start')
            return false;
        if (indexes.col < this.cells.length - 1 && this.cells[indexes.col + 1][indexes.row].getType() === 'start')
            return false;

        return true;
    }

    testForEndReachable() {
        this.endPos = null;
        let startCell = null;
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'start':
                        startCell = cell;
                        break;
                }
            }
        }
        this.testedCell = [];
        this.findPathToEnd(startCell);
        return !(this.endPos === null && this.isStartDrawn && this.isStopDrawn);
    }

    findPathToEnd(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'end')
            this.endPos = cell.getTableIndex();

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToEnd(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToEnd(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToEnd(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToEnd(this.cells[indexes.col + 1][indexes.row]);
    }

    testForCoinsReachables() {
        let coinCells = [];
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'coin':
                        coinCells.push(cell);
                        break;
                }
            }
        }
        let foundCount = 0;
        for (let coinCell of coinCells) {
            this.testedCell = [];
            this.pathToHeroFound = false;
            this.findPathToHero(coinCell);
            if (this.pathToHeroFound)
                foundCount++;
        }
        this.testedCell = [];
        return foundCount === coinCells.length;
    }

    findPathToHero(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'start')
            this.pathToHeroFound = true;

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToHero(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToHero(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToHero(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToHero(this.cells[indexes.col + 1][indexes.row]);
    }

    drawPath(e) {
        let cell = this.findCellForEvent(e);
        if (!this.lastDrawCell && cell.getType() !== 'start')
            return;
        if (!this.lastDrawCell && cell.getType() === 'start') {
            this.lastDrawCell = cell;
            return;
        }
        if (cell.getType() && cell.getType() !== 'coin' && cell.getType() !== 'end')
            return;

        let currentIndexes = cell.getTableIndex();

        if (Math.abs(currentIndexes.col - this.lastDrawCell.getTableIndex().col) + Math.abs(currentIndexes.row - this.lastDrawCell.getTableIndex().row) > 1)
            return;
        let direction = '';
        if (currentIndexes.col < this.lastDrawCell.getTableIndex().col)
            direction = 'left';
        else if (currentIndexes.col > this.lastDrawCell.getTableIndex().col)
            direction = 'right';
        else if (currentIndexes.row < this.lastDrawCell.getTableIndex().row)
            direction = 'up';
        else if (currentIndexes.row > this.lastDrawCell.getTableIndex().row)
            direction = 'down';
        else
            return;
        if (this.lastDrawCell.getType() !== 'start') {
            this.lastDrawCell.setSprite(this.refGame.global.resources.getImage(direction));
            this.lastDrawCell.setType(direction);
        }
        this.lastDrawCell = cell;

        if (cell.getType() === 'coin')
            this.coinRecovered++;

        if (cell.getType() !== 'end') {
            cell.setSprite(this.refGame.global.resources.getImage('current_' + direction));
            cell.setType('current_' + direction);
            this.moveCount++;
        } else if (cell.getType() === 'end') {
            if (this.coinCount === this.coinRecovered)
                this.refGame.global.util.showAlert(
                    'success',
                    this.refGame.global.resources.getOtherText('victory', {count: this.moveCount + 2}),
                    this.refGame.global.resources.getOtherText('success'), undefined,
                    this.newGame.bind(this)
                );
            else
                this.refGame.global.util.showAlert(
                    'error',
                    this.refGame.global.resources.getOtherText('defeat'),
                    this.refGame.global.resources.getOtherText('failure'), undefined, function () {
                        this.newGame();
                    }.bind(this));

        }
        if (this.checkDefeat(cell)) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('blocked'),
                this.refGame.global.resources.getOtherText('failure'),
                undefined,
                this.newGame.bind(this));
        }
    }

    findCellForEvent(e) {
        this.refGame.global.util.globalizeEvent(e);
        let cellSize = 600 / this.actualSize;
        let col = Math.floor((e.clientX - this.refGame.global.canvas.OFFSETLEFT) / cellSize);
        let row = Math.floor((e.clientY - this.refGame.global.canvas.OFFSETTOP) / cellSize);
        if (col >= this.actualSize)
            col = this.actualSize - 1;
        if (row >= this.actualSize)
            row = this.actualSize - 1;
        if (col < 0)
            col = 0;
        if (row < 0)
            row = 0;
        return this.cells[col][row];
    }

    checkDefeat(cell) {
        let indexes = cell.getTableIndex();
        return !((indexes.col > 0 && (!this.cells[indexes.col - 1][indexes.row].getType() || this.cells[indexes.col - 1][indexes.row].getType() === 'coin'))
            || (indexes.row > 0 && (!this.cells[indexes.col][indexes.row - 1].getType() || this.cells[indexes.col][indexes.row - 1].getType() === 'coin'))
            || (indexes.row < this.cells[indexes.col].length - 1 && (!this.cells[indexes.col][indexes.row + 1].getType() || this.cells[indexes.col][indexes.row + 1].getType() === 'coin'))
            || (indexes.col < this.cells.length - 1 && (!this.cells[indexes.col + 1][indexes.row].getType() || this.cells[indexes.col + 1][indexes.row].getType() === 'coin')));
    }
}
