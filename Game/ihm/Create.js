/**
 * @classdesc IHM de la création d'exercice
 * @author Sturzenegger Erwan
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */

class Create extends Interface {

    constructor(refGame) {

        super(refGame, "create");
        this.refGame = refGame;


        this.currentText = 'createModeIntro';
        this.cells = [];
        this.steps = [4, 8, 12];
        this.actualSize = 4;
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;

        this.screens = {
            title: {
                mainTitle: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'}),
                startButton: new Button(300, 400, '', 0x007bff, 0xFFFFFF, true)
            },
            levelResume: {
                resumeTitle: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'}),
                nextLevelbtn: new Button(500, 550, '', 0x007bff, 0xFFFFFF, true),
                stopLevelBtn: new Button(300, 550, '', 0x6c757d, 0xFFFFFF, true),
                deleteLevelBtn: new Button(100, 550, '', 0xdc3545, 0xFFFFFF, true),
            },
            game: {
                cellsContainer: new PIXI.Container()
            },
            submission: {
                submissionTitle: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                btnSubmit: new Button(300, 200, '', 0x28a745, 0xFFFFFF, false, 250),
                btnDelete: new Button(300, 300, '', 0xdc3545, 0xFFFFFF, false, 250),
                btnTest: new Button(300, 400, '', 0x007bff, 0xFFFFFF, false, 250)
            }
        };

        //Title setup
        this.screens.title.mainTitle.anchor.set(0.5);
        this.screens.title.mainTitle.x = 300;
        this.screens.title.mainTitle.y = 150;
        this.screens.title.startButton.setOnClick(this.startCreateMode.bind(this));

        //LevelResume setup
        this.screens.levelResume.resumeTitle.anchor.set(0.5);
        this.screens.levelResume.resumeTitle.x = 300;
        this.screens.levelResume.resumeTitle.y = 50;
        this.screens.levelResume.nextLevelbtn.setOnClick(this.handleSaveLevel.bind(this));
        this.screens.levelResume.stopLevelBtn.setOnClick(this.handleStopInsertion.bind(this));
        this.screens.levelResume.deleteLevelBtn.setOnClick(this.handleDeleteLevel.bind(this));

        //submission setup
        this.screens.submission.submissionTitle.anchor.set(0.5);
        this.screens.submission.submissionTitle.x = 300;
        this.screens.submission.submissionTitle.y = 100;
        this.screens.submission.btnDelete.setOnClick(this.handleDeleteExercice.bind(this));
        this.screens.submission.btnTest.setOnClick(this.handleTestExercice.bind(this));
        this.screens.submission.btnSubmit.setOnClick(this.handleSubmitExercice.bind(this));


        this.levels = {
            name: "",
            levels: []
        };
        this.currentLevel = {};

        setInterval(this.updateGif.bind(this), 1);

    }


    show(fromTrain = false) {
        if (!fromTrain) {
            this.currentLevel = {};
            this.levels = {
                name: "",
                levels: []
            };

            this.updateScreen('title', true);
            this.updateScreen('game', false);
            this.updateScreen('levelResume', false);
            this.updateScreen('submission', false);
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
            this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'sizeSlider', 'btnEnd', 'btnContinue');
            this.clear();
            for (let screen of Object.keys(this.screens))
                for (let comp of Object.keys(this.screens[screen]))
                    this.elements.push(this.screens[screen][comp]);
            this.refreshLang(this.refGame.global.resources.getLanguage());
            this.showTitle();
        }
        this.init();
    }


    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.screens.title.mainTitle.text = this.refGame.global.resources.getOtherText('welcome');
        this.screens.title.startButton.setText(this.refGame.global.resources.getOtherText('start'));
        this.screens.levelResume.resumeTitle.setText(this.refGame.global.resources.getOtherText('saveTitle'));
        this.screens.levelResume.deleteLevelBtn.setText(this.refGame.global.resources.getOtherText('deleteLevel'));
        this.screens.levelResume.nextLevelbtn.setText(this.refGame.global.resources.getOtherText('nextLevel'));
        this.screens.levelResume.stopLevelBtn.setText(this.refGame.global.resources.getOtherText('stopInsertion'));
        this.screens.submission.btnDelete.setText(this.refGame.global.resources.getOtherText('deleteExercice'));
        this.screens.submission.btnSubmit.setText(this.refGame.global.resources.getOtherText('submitExercice'));
        this.screens.submission.btnTest.setText(this.refGame.global.resources.getOtherText('testExercice'));
        this.screens.submission.submissionTitle.text = this.refGame.global.resources.getTutorialText('confirmExercice');
    }

    showTitle() {
        this.currentText = 'createModeIntro';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    updateScreen(screen, visble) {
        for (let component of Object.keys(this.screens[screen])) {
            if (this.screens[screen][component] instanceof Asset)
                this.screens[screen][component].setVisible(visble);
            else if (this.screens[screen][component].hasOwnProperty('visible'))
                this.screens[screen][component].visible = visble;
        }
    }

    startCreateMode() {
        this.levels = {
            name: "",
            levels: []
        };
        this.newGame();
    }

    showLevelResume() {
        this.prepareLevel();
        this.generateCells(400, 100, 100);
        this.loadCurrentLevel();
        this.updateScreen('levelResume', true);
        this.updateScreen('game', true);
        this.refGame.global.util.hideTutorialInputs('next');
    }

    handleSaveLevel() {
        this.levels.levels.push(this.currentLevel);
        this.currentLevel = {};
        this.newGame();
    }

    handleDeleteLevel() {
        this.currentLevel = {};
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('confirmDeletion'),
            text: this.refGame.global.resources.getOtherText('confirmDeletionText'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: this.refGame.global.resources.getOtherText('yes'),
            cancelButtonText: this.refGame.global.resources.getOtherText('no')
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('deleted'),
                    this.refGame.global.resources.getOtherText('deletedLevelText'),
                    'success'
                );
                this.newGame();
            }
        });
    }

    handleStopInsertion() {
        this.levels.levels.push(this.currentLevel);
        this.currentLevel = {};
        this.currentText = 'chooseOption';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.updateScreen('levelResume', false);
        this.updateScreen('game', false);
        this.updateScreen('submission', true);
    }

    prepareLevel() {
        let level = {
            size: this.actualSize,
            cells: {
                start: {},
                stop: {},
                walls: [],
                mandatories: []
            }
        };
        for (let col of this.cells)
            for (let row of col) {
                if (!row.getType())
                    continue;
                switch (row.getType()) {
                    case 'start':
                        level.cells.start.x = row.getTableIndex().col;
                        level.cells.start.y = row.getTableIndex().row;
                        break;
                    case 'end':
                        level.cells.stop.x = row.getTableIndex().col;
                        level.cells.stop.y = row.getTableIndex().row;
                        break;
                    case 'wall':
                        level.cells.walls.push({
                            x: row.getTableIndex().col,
                            y: row.getTableIndex().row
                        });
                        break;
                    case 'coin':
                        level.cells.mandatories.push({
                            x: row.getTableIndex().col,
                            y: row.getTableIndex().row
                        });
                        break;
                }
            }
        this.currentLevel = level;
    }

    loadCurrentLevel() {
        this.cells[this.currentLevel.cells.start.x][this.currentLevel.cells.start.y].setType('start');
        this.cells[this.currentLevel.cells.start.x][this.currentLevel.cells.start.y].setSprite(this.refGame.global.resources.getImage('hero'));

        this.cells[this.currentLevel.cells.stop.x][this.currentLevel.cells.stop.y].setType('end');
        this.cells[this.currentLevel.cells.stop.x][this.currentLevel.cells.stop.y].setSprite(this.refGame.global.resources.getImage('chest'));

        for (let wall of this.currentLevel.cells.walls) {
            this.cells[wall.x][wall.y].setType('wall');
            this.cells[wall.x][wall.y].setSprite(this.refGame.global.resources.getImage('bat'));
        }

        for (let coin of this.currentLevel.cells.mandatories) {
            this.cells[coin.x][coin.y].setType('coin');
            this.cells[coin.x][coin.y].setSprite(this.refGame.global.resources.getImage('coin'));
        }
    }

    newGame() {
        this.updateScreen('title', false);
        this.updateScreen('levelResume', false);
        this.updateScreen('game', true);
        this.currentText = 'startCreate';
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.refGame.global.util.showTutorialInputs('next');
        this.cells = [];
        this.currentLevel = {};
        this.steps = [4, 8, 12];
        this.actualSize = this.steps[this.refGame.global.resources.getDegre() - 1];
        this.isStartDrawn = false;
        this.isStopDrawn = false;
        this.areWallsDrawn = false;
        this.areMandatoryDrawn = false;
        this.generateCells();
    }

    generateCells(forcedSize = 600, forcedX = 0, forcedY = 0) {
        var sideX = forcedSize / this.actualSize;
        var sideY = forcedSize / this.actualSize;
        var cells = [];
        this.screens.game.cellsContainer.removeChildren();
        var x = forcedX, y = forcedY;
        for (var i = 0; i < this.actualSize; i++) {
            y = forcedY;
            cells[i] = [];
            for (var j = 0; j < this.actualSize; j++) {
                var curCell = new Cellule(x, y, sideX, sideY, this.refGame.global.resources.getImage('cell'), this.handleCellClick.bind(this));
                curCell.setTableIndex(i, j);
                cells[i].push(curCell);
                for (let el of curCell.getPixiChildren())
                    this.screens.game.cellsContainer.addChild(el);
                y += sideY;
            }
            x += sideX;
        }
        this.cells = cells;
    }

    updateGif() {
        for (let col of this.cells)
            for (let cell of col)
                cell.updateGif();
    }

    handleCellClick(cell) {
        if (cell.containSprite())
            return;
        if (!this.isStartDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('hero'));
            cell.setType('start');
            this.isStartDrawn = true;
            this.currentText = 'end';
        } else if (!this.isStopDrawn) {
            if (this.testForStopPoint(cell)) {
                this.isStopDrawn = true;
                cell.setSprite(this.refGame.global.resources.getImage('chest'));
                cell.setType('end');
                this.currentText = 'coin';
            } else {
                this.refGame.global.util.showAlert(
                    'warning',
                    this.refGame.global.resources.getOtherText('tooclose'),
                    this.refGame.global.resources.getOtherText('error'),
                    undefined,
                    undefined);
            }
        } else if (!this.areMandatoryDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('coin'));
            cell.setType('coin');
            this.coinCount++;
        } else if (!this.areWallsDrawn) {
            cell.setSprite(this.refGame.global.resources.getImage('bat'));
            cell.setType('wall');
        }
        if (!this.testForEndReachable() || !this.testForCoinsReachables()) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('notfeasable'),
                this.refGame.global.resources.getOtherText('error'),
                undefined,
                this.newGame.bind(this));
        }

        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    handleNext() {
        if (this.isStartDrawn && this.isStopDrawn) {
            if (!this.areMandatoryDrawn) {
                this.areMandatoryDrawn = true;
                this.currentText = 'wall';
            } else if (!this.areWallsDrawn) {
                this.areWallsDrawn = true;
                this.updateScreen('game', false);
                this.showLevelResume();
                this.currentText = 'chooseOption';
            }
        }
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    testForStopPoint(cell) {
        let indexes = cell.getTableIndex();

        if (indexes.col > 0 && this.cells[indexes.col - 1][indexes.row].getType() === 'start')
            return false;
        if (indexes.row > 0 && this.cells[indexes.col][indexes.row - 1].getType() === 'start')
            return false;
        if (indexes.row < this.cells[indexes.col].length - 1 && this.cells[indexes.col][indexes.row + 1].getType() === 'start')
            return false;
        if (indexes.col < this.cells.length - 1 && this.cells[indexes.col + 1][indexes.row].getType() === 'start')
            return false;

        return true;
    }

    testForEndReachable() {
        this.endPos = null;
        let startCell = null;
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'start':
                        startCell = cell;
                        break;
                }
            }
        }
        this.testedCell = [];
        this.findPathToEnd(startCell);
        return !(this.endPos === null && this.isStartDrawn && this.isStopDrawn);
    }

    findPathToEnd(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'end')
            this.endPos = cell.getTableIndex();

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToEnd(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToEnd(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToEnd(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToEnd(this.cells[indexes.col + 1][indexes.row]);
    }

    testForCoinsReachables() {
        let coinCells = [];
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                let cell = this.cells[i][j];
                switch (cell.getType()) {
                    case 'coin':
                        coinCells.push(cell);
                        break;
                }
            }
        }
        let foundCount = 0;
        for (let coinCell of coinCells) {
            this.testedCell = [];
            this.pathToHeroFound = false;
            this.findPathToHero(coinCell);
            if (this.pathToHeroFound)
                foundCount++;
        }
        this.testedCell = [];
        return foundCount === coinCells.length;
    }

    findPathToHero(cell) {
        if (this.testedCell.indexOf(cell) >= 0)
            return;
        this.testedCell.push(cell);
        if (cell.getType() === 'wall')
            return;
        if (cell.getType() === 'start')
            this.pathToHeroFound = true;

        let indexes = cell.getTableIndex();
        if (indexes.col > 0)
            this.findPathToHero(this.cells[indexes.col - 1][indexes.row]);
        if (indexes.row > 0)
            this.findPathToHero(this.cells[indexes.col][indexes.row - 1]);
        if (indexes.row < this.cells[indexes.col].length - 1)
            this.findPathToHero(this.cells[indexes.col][indexes.row + 1]);
        if (indexes.col < this.cells.length - 1)
            this.findPathToHero(this.cells[indexes.col + 1][indexes.row]);
    }

    handleDeleteExercice() {
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('confirmDeletion'),
            text: this.refGame.global.resources.getOtherText('confirmDeletionText'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33d33',
            confirmButtonText: this.refGame.global.resources.getOtherText('yes'),
            cancelButtonText: this.refGame.global.resources.getOtherText('no')
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('deleted'),
                    this.refGame.global.resources.getOtherText('deletedExerciceText'),
                    'success'
                );
                this.show();
            }
        });
    }

    handleTestExercice() {
        this.refGame.trainMode.init(false);
        this.refGame.trainMode.show(this.levels);
    }

    handleSubmitExercice() {
        this.refGame.global.resources.saveExercice(JSON.stringify(this.levels), function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );
            this.show();
        }.bind(this));
    }
}
