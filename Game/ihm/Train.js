/**
 * @classdesc IHM de la création d'exercice
 * @author Sturzenegger Erwan
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */

class Train extends Interface {
    constructor(refGame) {

        super(refGame, "train");
        this.refGame = refGame;

        this.tryMode = false;

        this.currentText = 'start';
        this.cells = [];
        this.steps = [4, 6, 8, 10, 12];
        this.actualSize = 4;
        this.lastDrawCell = null;
        this.coinCount = 0;
        this.coinRecovered = 0;
        this.moveCount = 0;
        this.isDrawing = false;
        this.goodLevels = 0;
        this.currentLevel = 0;
        this.exercice = null;
        this.evaluate = false;
        this.cellsContainer = new PIXI.Container();
        this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.startButton.setOnClick(this.newGame.bind(this));
        this.backToCreateModeBtn = new Button(100, 575, '', 0x007bff, 0xFFFFFF, false, 200);
        this.backToCreateModeBtn.setOnClick(this.backToCreateMode.bind(this));
        this.title.x = 300;
        this.title.y = 150;
        this.title.anchor.set(0.5);
        this.elements.push(this.cellsContainer);
        this.elements.push(this.title);
        this.elements.push(this.startButton);
        this.elements.push(this.backToCreateModeBtn);
        setInterval(this.updateGif.bind(this), 1);

    }


    show(evaluate, forcedExerice = null) {
        if (forcedExerice)
            this.tryMode = true;
        else
            this.tryMode = false;
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousedown', 'touchstart'], this._eventStart.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mouseup', 'mouseout', 'touchend', 'touchleave'], this._eventStop.bind(this));
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('canvas'), ['mousemove', 'touchmove'], this._eventMove.bind(this));
        this.evaluate = evaluate;
        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'sizeSlider', 'btnEnd', 'btnContinue');
        if (this.tryMode) {
            this.backToCreateModeBtn.setVisible(true);
            this.newGame(forcedExerice);
        } else {
            this.showTitle();
            this.backToCreateModeBtn.setVisible(false);
        }
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
        this.setupSignalement();
    }

    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        if (this.evaluate)
            this.title.text = this.refGame.global.resources.getOtherText('evalModeTitle');
        else
            this.title.text = this.refGame.global.resources.getOtherText('trainModeTitle');
        this.startButton.setText(this.refGame.global.resources.getOtherText('startGame'));
        this.backToCreateModeBtn.setText(this.refGame.global.resources.getOtherText('backToCreateMode'));
    }

    showTitle() {
        this.cellsContainer.visible = false;
        this.title.visible = true;
        this.startButton.setVisible(true);
    }

    newGame(forcedExerice = null) {
        if (forcedExerice)
            this.exercice = forcedExerice;
        else {
            this.exercice = this.refGame.global.resources.getExercice();
            this.currentExerciceId = this.exercice.id;
            this.exercice = JSON.parse(this.exercice.exercice);
            this.refGame.inputs.signal.style.display = 'block';
        }
        if (this.evaluate)
            this.refGame.global.statistics.addStats(2);

        this.title.visible = false;
        this.startButton.setVisible(false);
        this.cellsContainer.visible = true;

        this.currentLevel = 0;
        this.goodLevels = 0;
        this.nextLevel();
    }

    nextLevel() {
        if (this.currentLevel >= this.exercice.levels.length) {
            this.showEndExercice();
        } else {
            this.currentText = 'gameTrain';
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
            this.cells = [];
            this.steps = [4, 6, 8, 10, 12];
            this.actualSize = this.exercice.levels[this.currentLevel].size;
            this.lastDrawCell = null;
            this.coinCount = 0;
            this.coinRecovered = 0;
            this.moveCount = 0;
            this.isDrawing = false;
            this.generateCells();
            this.fillExercice(this.exercice.levels[this.currentLevel]);
            this.currentLevel++;
        }
    }

    showEndExercice() {

        if (!this.tryMode) {
            if (this.evaluate) {
                let mark = 0;
                let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
                let perc = (this.goodLevels / this.exercice.levels.length) * 100;
                for (let b of barem) {
                    if (b.min <= perc && b.max >= perc) {
                        mark = b.note;
                    }
                }
                this.refGame.global.statistics.updateStats(mark);
            } else {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('exerciceEnd'),
                    this.refGame.global.resources.getOtherText('exerciceEndText', {
                        count: this.goodLevels,
                        tot: this.exercice.levels.length
                    }),
                    'success'
                );
            }
            this.showTitle();
        } else
            this.backToCreateMode();
    }

    _eventStart(e) {
        e.preventDefault();
        this.isDrawing = true;
    }

    _eventMove(e) {
        e.preventDefault();
        if (this.isDrawing)
            this.drawPath(e);
    }

    _eventStop(e) {
        e.preventDefault();
        this.isDrawing = false;
    }


    fillExercice(exercice) {
        this.cells[exercice.cells.start.x][exercice.cells.start.y].setType('start');
        this.cells[exercice.cells.start.x][exercice.cells.start.y].setSprite(this.refGame.global.resources.getImage('hero'));

        this.cells[exercice.cells.stop.x][exercice.cells.stop.y].setType('end');
        this.cells[exercice.cells.stop.x][exercice.cells.stop.y].setSprite(this.refGame.global.resources.getImage('chest'));

        for (let wall of exercice.cells.walls) {
            this.cells[wall.x][wall.y].setType('wall');
            this.cells[wall.x][wall.y].setSprite(this.refGame.global.resources.getImage('bat'));
        }

        for (let coin of exercice.cells.mandatories) {
            this.cells[coin.x][coin.y].setType('coin');
            this.cells[coin.x][coin.y].setSprite(this.refGame.global.resources.getImage('coin'));
            this.coinCount++;
        }

    }

    generateCells() {
        var sideX = (this.tryMode ? 550 : 600) / this.actualSize;
        var sideY = (this.tryMode ? 550 : 600) / this.actualSize;
        var cells = [];
        this.cellsContainer.removeChildren();
        var x = 0, y = 0;
        for (var i = 0; i < this.actualSize; i++) {
            y = 0;
            cells[i] = [];
            for (var j = 0; j < this.actualSize; j++) {
                var curCell = new Cellule(x, y, sideX, sideY, this.refGame.global.resources.getImage('cell'), undefined);
                curCell.setTableIndex(i, j);
                cells[i].push(curCell);
                for (let el of curCell.getPixiChildren())
                    this.cellsContainer.addChild(el);
                y += sideY;
            }
            x += sideX;
        }
        this.cells = cells;
    }

    updateGif() {
        for (let col of this.cells)
            for (let cell of col)
                cell.updateGif();
    }


    drawPath(e) {
        let cell = this.findCellForEvent(e);
        if (!this.lastDrawCell && cell.getType() !== 'start')
            return;
        if (!this.lastDrawCell && cell.getType() === 'start') {
            this.lastDrawCell = cell;
            return;
        }
        if (cell.getType() && cell.getType() !== 'coin' && cell.getType() !== 'end')
            return;

        let currentIndexes = cell.getTableIndex();

        if (Math.abs(currentIndexes.col - this.lastDrawCell.getTableIndex().col) + Math.abs(currentIndexes.row - this.lastDrawCell.getTableIndex().row) > 1)
            return;
        let direction = '';
        if (currentIndexes.col < this.lastDrawCell.getTableIndex().col)
            direction = 'left';
        else if (currentIndexes.col > this.lastDrawCell.getTableIndex().col)
            direction = 'right';
        else if (currentIndexes.row < this.lastDrawCell.getTableIndex().row)
            direction = 'up';
        else if (currentIndexes.row > this.lastDrawCell.getTableIndex().row)
            direction = 'down';
        else
            return;
        if (this.lastDrawCell.getType() !== 'start') {
            this.lastDrawCell.setSprite(this.refGame.global.resources.getImage(direction));
            this.lastDrawCell.setType(direction);
        }
        this.lastDrawCell = cell;

        if (cell.getType() === 'coin')
            this.coinRecovered++;
        if (cell.getType() !== 'end') {
            cell.setSprite(this.refGame.global.resources.getImage('current_' + direction));
            cell.setType('current_' + direction);
            this.moveCount++;
        } else if (cell.getType() === 'end') {
            if (this.coinCount === this.coinRecovered) {
                this.goodLevels++;
                this.refGame.global.util.showAlert(
                    'success',
                    this.refGame.global.resources.getOtherText('victory', {count: this.moveCount + 2}),
                    this.refGame.global.resources.getOtherText('success'), undefined,
                    this.nextLevel.bind(this)
                );
            } else
                this.refGame.global.util.showAlert(
                    'error',
                    this.refGame.global.resources.getOtherText('defeat'),
                    this.refGame.global.resources.getOtherText('failure'), undefined, this.nextLevel.bind(this));

        }
        if (this.checkDefeat(cell)) {
            this.refGame.global.util.showAlert(
                'error',
                this.refGame.global.resources.getOtherText('blocked'),
                this.refGame.global.resources.getOtherText('failure'),
                undefined,
                this.nextLevel.bind(this));
        }
    }

    findCellForEvent(e) {
        this.refGame.global.util.globalizeEvent(e);
        let cellSize = (this.tryMode ? 550 : 600) / this.actualSize;
        let col = Math.floor((e.clientX - this.refGame.global.canvas.OFFSETLEFT) / cellSize);
        let row = Math.floor((e.clientY - this.refGame.global.canvas.OFFSETTOP) / cellSize);
        if (col >= this.actualSize)
            col = this.actualSize - 1;
        if (row >= this.actualSize)
            row = this.actualSize - 1;
        if (col < 0)
            col = 0;
        if (row < 0)
            row = 0;
        return this.cells[col][row];
    }

    checkDefeat(cell) {
        let indexes = cell.getTableIndex();
        return !((indexes.col > 0 && (!this.cells[indexes.col - 1][indexes.row].getType() || this.cells[indexes.col - 1][indexes.row].getType() === 'coin'))
            || (indexes.row > 0 && (!this.cells[indexes.col][indexes.row - 1].getType() || this.cells[indexes.col][indexes.row - 1].getType() === 'coin'))
            || (indexes.row < this.cells[indexes.col].length - 1 && (!this.cells[indexes.col][indexes.row + 1].getType() || this.cells[indexes.col][indexes.row + 1].getType() === 'coin'))
            || (indexes.col < this.cells.length - 1 && (!this.cells[indexes.col + 1][indexes.row].getType() || this.cells[indexes.col + 1][indexes.row].getType() === 'coin')));
    }

    backToCreateMode() {
        this.refGame.createMode.show(true);
    }

    setupSignalement() {
        this.refGame.inputs.signal.onclick = function () {
            Swal.fire({
                title: this.refGame.global.resources.getOtherText('signalementTitle'),
                cancelButtonText: this.refGame.global.resources.getOtherText('signalementCancel'),
                showCancelButton: true,
                html:
                    '<label>' + this.refGame.global.resources.getOtherText('signalementReason') + '</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                    '<label>' + this.refGame.global.resources.getOtherText('signalementPassword') + '</label><input type="password" id="swal-password" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-reason').val(),
                            $('#swal-password').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-reason').focus()
                }
            }).then(function (result) {
                if (result && result.value) {
                    this.refGame.global.resources.signaler(result.value[0], result.value[1], this.currentExerciceId, function (res) {
                        Swal.fire(res.message, '', res.type);
                        if (res.type === 'success') {
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);
    }
}
