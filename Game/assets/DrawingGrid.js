/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
