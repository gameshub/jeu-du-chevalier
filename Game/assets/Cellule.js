class Cellule extends Asset {


    constructor(x, y, width, height, background, onclick) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.sprite = null;
        this.background = background;
        this.container = new PIXI.Container();
        this.container.width = 600;
        this.container.height = 600;
        this.onclick = onclick;
        this.type = '';
        this.init();
        this.tableIndex = {
            col: -1,
            row: -1
        }
    }

    setSprite(image) {
        this.sprite = {
            image: image,
            currentFrame: 0,
            speed: 100,
            lastUpdate: 0,
            sprite: null,
            size: 0,
            offset: 0
        };
        this.init();
    }

    getPixiChildren() {
        return [this.container];
    }

    init() {
        this.container.removeChildren();
        let backgrd = new PIXI.Sprite.fromImage(this.background.image.src);
        backgrd.anchor.x = 0;
        backgrd.anchor.y = 0;
        backgrd.x = this.x;
        backgrd.y = this.y;
        backgrd.width = this.width;
        backgrd.height = this.height;
        backgrd.interactive = true;
        backgrd.on('pointerdown', function () {
            if (this.onclick)
                this.onclick(this);
        }.bind(this));

        this.container.addChild(backgrd);

        if (this.containSprite()) {
            let image = this.sprite.image;
            let imageHeight = this.height < image.getHeight() ? this.height : image.getHeight();
            while (imageHeight / this.height < 0.6)
                imageHeight++;
            while (imageHeight / this.height > 0.6)
                imageHeight--;
            let imageWidth = (imageHeight / image.getHeight()) * image.getWidth() * image.getFrames();
            let sprite = new PIXI.Sprite.fromImage(image.image.src);
            sprite.anchor.x = 0;
            sprite.anchor.y = 0;
            sprite.x = this.x + (this.width - imageWidth / image.getFrames()) / 2;
            sprite.y = this.y + this.height * 0.2;
            sprite.width = imageWidth;
            sprite.height = imageHeight;
            if (image.getFrames() !== 1) {
                let mask = new PIXI.Graphics();
                mask.drawRect(this.x + (this.width - imageWidth / image.getFrames()) / 2, this.y, imageWidth / image.getFrames(), this.height);
                sprite.mask = mask;
            }
            this.sprite.sprite = sprite;
            this.sprite.size = imageWidth / image.getFrames();
            this.sprite.offset = this.x + (this.width - imageWidth / image.getFrames()) / 2;
            this.container.addChild(sprite);
        }

    }

    updateGif() {
        if (!this.containSprite())
            return;
        if (this.sprite.image.getFrames() === 1)
            return;
        let date = new Date().getTime();
        if (this.sprite.speed < (date - this.sprite.lastUpdate)) {
            this.sprite.lastUpdate = date;
            this.updateSprite(this.sprite);
        }
    }

    updateSprite(image) {
        image.currentFrame++;
        if (image.image.getFrames() < image.currentFrame + 1) {
            image.currentFrame = 1;
            image.sprite.x = image.offset;
        }
        image.sprite.x = image.sprite.x - image.size;
    }

    getY() {
        return this.y
    }

    getX() {
        return this.x
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.width;
    }

    containSprite() {
        return this.sprite;
    }

    getType() {
        return this.type;
    }

    setType(type) {
        this.type = type;
    }

    getHeight() {
        return this.getHeight();
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getTableIndex() {
        return this.tableIndex;
    }

    setTableIndex(col, row) {
        this.tableIndex.col = col;
        this.tableIndex.row = row;
    }
}