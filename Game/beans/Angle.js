
/**
 * @classdesc Objet angle
 * @author Vincent Audergon
 * @version 1.0
 */
class Angle {

    /**
     * Constructeur de l'objet Angle
     * @param {double} alpha L'angle alpha (intérieur au polygone)
     * @param {double} beta L'angle beta (exterieur au polygone)
     * @param {string[]} points Les points concercées par l'angle (ABC par ex.)
     * @example
     *
     *      new Angle(90,270,['A','B','C'])
     */
    constructor(alpha, beta, points){
        this.alpha = alpha;
        this.beta = beta;
        this.points = points;
    }

}