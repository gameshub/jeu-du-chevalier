/**
 * @classdesc Objet Line, représente une droite en mathématiques
 * @author Vincent Audergon
 * @version 1.0
 */
class Line {

    /**
     * Constructeur de l'objet Line. Il est possible de créer la droite à partir
     * d'un angle et d'un point ou des attributs a et b d'une fonction affine.
     * @param {double|double} angle L'angle de la fonction en **radians** | a si fromAngle est faux
     * @param {Point|double} p1 Un {@link Point} de la fonction | b si fromAngle est faux
     * @param {boolean} [fromAngle=true] Argument facultatif, si la droite est calculée
     *                            à partir d'un angle ou pas, vrai par défaut
     *
     * @example
     *
     *      let l1 = new Line(1,1, false); //A partir de a et b
     *      let l2 = new Line(45,new Point(1,1)) //A partir d'un angle
     */
    constructor(angle, p1, fromAngle = true) {
        if (fromAngle) {
            if (angle % Math.PI !== Math.PI / 2) {
                this.a = Math.round(Math.tan(angle) * 100) / 100;
                this.b = Math.round((p1.y - this.a * p1.x) * 100) / 100;
            } else {
                this.a = Infinity;
                this.b = p1.x;
            }
        } else {
            this.a = angle;
            this.b = p1;
        }
    }

    /**
     * Calcule la coordonnée y en fonction de la coordonnée x
     * Selon l'équation *y = ax+b*
     * @param {double} x La coordonnée x
     * @return {double} La coordonnée y
     */
    calcY(x) {
        if (this.a != Infinity) {
            return this.a * x + this.b;
        } else {
            return x === this.b ? Infinity : NaN;
        }
    }

    /**
     * Vérifie si un point est sur la droite ou non
     * @param {Point} point Le {@link Point} à vérifier
     * @return {boolean} Si le point est sur la droite
     */
    containsPoint(point) {
        return this.calcY(point.x) === point.y || this.calcY(point.x) === Infinity;
    }
}
