define(["require", "exports"], function (require, exports) {
    "use strict";
    console['success'] = function (text) {
        this.log("%c " + text, 'color: limegreen;');
    };
    var Log = (function () {
        function Log(type, message) {
            this.message = message;
            this.type = type;
            this.time = new Date();
        }
        Log.prototype.show = function () {
            console[this.type](this.formatDate(this.time) + " " + this.message);
        };
        Log.prototype.formatDate = function (date) {
            var pad = function (num) { return ('00' + num).slice(-2); };
            return "[" + pad(date.getUTCDate()) + "/" + pad(date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + " - " + pad(date.getUTCHours() + 2) + ":" + pad(date.getUTCMinutes()) + ":" + pad(date.getSeconds()) + "." + date.getMilliseconds() + "]";
        };
        return Log;
    }());
    return Log;
});
